#include "fastTransform.h"

template <typename T>
void RandQRMatrix<T>::operator() ( T* x, T* out, T sign,  const bool transpose) const{
    typedef Eigen::Matrix<T, Eigen::Dynamic, 1> MatrixTd;
    typedef Eigen::Map<MatrixTd> MapTd;
    
    if (std::abs(sign - 1.0) > 1e-12) {
        
        int array_length = N;
        
        if (transpose) {
            array_length = M;
        }

        for (int i = 0; i < array_length; i++) {
            x[i] = sign*x[i];
        }
    }
     
    if (transpose) {
         
        MapTd x_vector(x, M); // Create Eigen vector
         
        MatrixTd out_vector = A.transpose()*x_vector; // Preform the matrix multiplication
        //std::cout << "Tranpose rows: " << out_vector.rows() << std::endl;
         
        // Copy the output vector to the output memory array
        std::memcpy(out, &out_vector(0), sizeof(T)*N);
         
    } else {
         
        MapTd x_vector(x, N); // Create Eigen vector
        
        MatrixTd out_vector = A*x_vector;// Preform the matrix multiplication
         
        // Copy the output vector to the output memory array
        std::memcpy(out, &out_vector(0), sizeof(T)*M);
         
    }
    
    if (std::abs(sign - 1.0) > 1e-12) {
        
        int array_length = N;
        
        if (transpose) {
            array_length = M;
        }

        for (int i = 0; i < array_length; i++) {
            x[i] = (1/sign)*x[i];
        }
    }
}



template class RandQRMatrix<double>;

template <typename T>
void CoinMatrix<T>::operator() ( T* x, T* out, T sign,  const bool transpose) const {
    typedef Eigen::Matrix<T, Eigen::Dynamic, 1> MatrixTd;
    typedef Eigen::Map<MatrixTd> MapTd;
    
    if (std::abs(sign - 1.0) > 1e-12) {
        
        int array_length = N;
        
        if (transpose) {
            array_length = M;
        }

        for (int i = 0; i < array_length; i++) {
            x[i] = sign*x[i];
        }
    }
     
    if (transpose) {
         
        MapTd x_vector(x, M); // Create Eigen vector
         
        MatrixTd out_vector = A.transpose()*x_vector; // Preform the matrix multiplication
        //std::cout << "Tranpose rows: " << out_vector.rows() << std::endl;
         
        // Copy the output vector to the output memory array
        std::memcpy(out, &out_vector(0), sizeof(T)*N);
         
    } else {
         
        MapTd x_vector(x, N); // Create Eigen vector
        
        MatrixTd out_vector = A*x_vector;// Preform the matrix multiplication
         
        // Copy the output vector to the output memory array
        std::memcpy(out, &out_vector(0), sizeof(T)*M);
         
    }
    
    if (std::abs(sign - 1.0) > 1e-12) {
        
        int array_length = N;
        
        if (transpose) {
            array_length = M;
        }

        for (int i = 0; i < array_length; i++) {
            x[i] = (1/sign)*x[i];
        }
    }
}

template class CoinMatrix<double>;


template <typename T>
void FastIDWT2WHT<T>::operator() ( T* x, T* out, T sign, const bool transpose) const {
    
    if ( transpose ) {
        // Then x is of size M
        // and out is of size N
         
        std::memset( out, 0, sizeof(T)*N );
         
        if (std::abs(sign - 1) < 1e-14) {
            for (int i = 0; i < M; i++) {
                out[idx[i]] = x[i];
            }
        } else {
            for (int i = 0; i < M; i++) {
                out[idx[i]] = sign*x[i];
            }
        }
         
        hadamardSequency(out, N);
         
        // divide by sqrt(N) to make the transform orthonormal  
        for (int i = 0; i < N; i++) {
            out[i] = out[i]/std::sqrt(N);
        }
         
        DWTPeriodic<T>(out, N, DB_h, DB_g, nu);  
         
    } else {
         
        // Then x is of size N
        // and out is of size M
        
        T  *x_tmp = new T[N]; 
        // Copy x into x_tmp 
        if (std::abs(sign - 1) < 1e-14) {
            for (int i = 0; i < N; i++) {
                x_tmp[i] = x[i];
            }
        } else {
            for (int i = 0; i < N; i++) {
                x_tmp[i] = sign*x[i];
            }
        }
        
        IDWTPeriodic<T>(x_tmp, N, DB_h, DB_g, nu);  
        hadamardSequency(x_tmp, N);
         
        // divide by sqrt(N) to make the transform orthonormal  
        for (int i = 0; i < N; i++) {
            x_tmp[i] = x_tmp[i]/std::sqrt(N);
        }
         
        // Copy the correct rows of x 
        for (int i = 0; i < M; i++) {
            out[i] = x_tmp[idx[i]];
        }
        delete [] x_tmp;
    }
}

template class FastIDWT2WHT<double>;

/*
    This operator is under construction.
*/
template <typename T>
void FastIDWT2WHT2d<T>::operator() ( T* x, T* out, T sign, const bool transpose) const {
    
    if ( transpose ) {
        // Then x is of size M
        // and out is of size N
         
        std::memset( out, 0, sizeof(T)*N );
         
        if (std::abs(sign - 1) < 1e-14) {
            for (int i = 0; i < M; i++) {
                out[idx[i]] = x[i];
            }
        } else {
            for (int i = 0; i < M; i++) {
                out[idx[i]] = sign*x[i];
            }
        }
         
        hadamard2d(out, m, n);
         
        // divide by sqrt(N) to make the transform orthonormal  
        for (int i = 0; i < N; i++) {
            out[i] = out[i]/((double)n);
        }
         
        DWT2d<T>(out, m,n, DB_h, DB_g, nres);  
         
    } else {
         
        // Then x is of size N
        // and out is of size M
        
        T  *x_tmp = new T[N]; 
         
        // Copy x into x_tmp 
        if (std::abs(sign - 1) < 1e-14) {
            for (int i = 0; i < N; i++) {
                x_tmp[i] = x[i];
            }
        } else {
            for (int i = 0; i < N; i++) {
                x_tmp[i] = sign*x[i];
            }
        }
        
        IDWT2d<T>(x_tmp, m,n, DB_h, DB_g, nres);  
        hadamard2d(x_tmp, m, n);
         
        // divide by n to make the transform orthonormal  
        for (int i = 0; i < N; i++) {
            x_tmp[i] = x_tmp[i]/n;
        }
         
        // Copy the correct rows of x 
        for (int i = 0; i < M; i++) {
            out[i] = x_tmp[idx[i]];
        }
        delete [] x_tmp;
    }
}

template class FastIDWT2WHT2d<double>;


/*

Generates a sparse vector with at most 'sparsity' number of non-zero elements.
The non-zero numbers will lie in the interval (-100, 100) and drawn from a
uniform probability distribution. If 'debug' is false the random generated
numbers will be seeded randomly each time the function is called. 

This function does not support complex numbers

*/
template <typename T>
void createSparseVector(T *x, const size_t sparsity, const size_t N, bool debug) {
    
    // Zero out all memory
    std::memset(x, 0, sizeof(double)*N);
    
    std::default_random_engine re;//re(rd());
    
    // Only seed the engine if not in debug mode 
    if (not debug) {
        std::random_device rd; // the seed device 
        re.seed(rd());
    }
    
    std::uniform_real_distribution<T> unif(-10, 10);
    std::uniform_int_distribution<int> idxGen(0, N-1);
    
    for (int i = 0; i < sparsity; i++) {
        int idx = idxGen(re);
        x[idx] = unif(re);
        //std::cout << std::setw(4) << idx << ": " << x[idx] << std::endl;
    }
    
}

template void createSparseVector<>(double *x, const size_t sparsity, const size_t N, bool debug);


