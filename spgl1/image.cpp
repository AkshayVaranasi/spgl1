/*

This program reads an image, sample it, and tries to reconstruct the image 
using the spgl1 algorithm.

*/



#include "stdafx.h"
#include "spgl1Algorithm.h"
#include "supportImage.h"
#include <opencv2/opencv.hpp>

using namespace cv;


int main(int argc, char *argv[]) {
     
    bool debug = true;
    
    // ******************  Read Algorithmic Constants **************************
    AlgorithmicConstants AC;
    AC.readConstants("config.txt");
    AC.printConstants(); 
     
    double alphaMin  = AC.getAlphaMin();
    double alphaMax  = AC.getAlphaMax();
    double delta     = AC.getDelta();
    double gamma     = AC.getGamma();
    double sigma     = AC.getSigma();
    double threshold = AC.getThreshold();
    double subsamplingFraction = AC.getSubsamplingFraction();
    int tau          = (int) AC.getTau();
    int vm           = AC.getVm();
    int LASSOMaxItr  = AC.getLASSOMaxItr();
    int lineSearchHistoryLength = AC.getLineSearchHistoryLength();
      
    Spgl1Algorithm spgl1(alphaMin, alphaMax, gamma,lineSearchHistoryLength);
      
    // *************************************************************************
     
    const Filter DB_h = get_DB_filter(vm);
    const Filter DB_g = createG(DB_h);
     
    cv::Mat image;
    image = cv::imread( "pictures/boat_1024.png", CV_LOAD_IMAGE_GRAYSCALE);
     
    if (image.depth() != CV_8U) {
        std::cerr << "Wrong data type" << std::endl;
        return 0;
    }
     
    double * imageAsVector = convertImageToVector(image); 
    size_t N = image.rows*image.cols;  
    size_t M = (size_t) std::round(N*subsamplingFraction); 
    std::cout << "M × N: " << M << " × " << N << std::endl;

    double im_norm1 = norm1<double>(imageAsVector, N);
    double im_norm2 = norm2<double>(imageAsVector, N);
    
    std::cout << "||image||_1: " << im_norm1 << std::endl; 
    std::cout << "||image||_2: " << im_norm2 << std::endl; 
     
    std::vector<int> scheme = create_rectangular_scheme(M,N); 
    int sum = 0;
    for (int i = 0; i < scheme.size(); i++) {
        sum += scheme[i];
    }
    std::vector<int> idx = create_rect_random_subsampling(scheme);
    
    double* b = hadamardSampleImage2d(imageAsVector, N, idx);
    
    FastIDWT2WHT2d<double> ft(vm, N, idx);
    
    std::cerr << "Starting rootFinding" << std::endl; 
    double *x1 = spgl1.rootFinding(ft, b, sigma, LASSOMaxItr); 
     
    IDWT2d<double>(x1,image.rows, image.cols, DB_h, DB_g, ft.get_nres());
     
    cv::Mat image_sparse =  convertVectorToImage(x1, image.rows,image.cols);
     
    char filename[50];
    std::sprintf(filename, "im_rec%d.png", tau);
    std::cout << "Saving file as: " << filename << std::endl; 
    cv::imwrite(filename, image_sparse);
     
    delete[] imageAsVector;  
    delete[] b;
     
    return 0; 
     
}




