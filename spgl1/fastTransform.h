#include "stdafx.h"
#include "supportImage.h"

/*

The superclass for which all matrix operators will interface in order to be 
applied in the spgl1Algorithm.

*/
template <typename T>
class FastTransformBase {
     
    protected:
        size_t N;
        size_t M; 
         
         
    public:    
         
         
        size_t rows() const {
            return M;    
        }
         
        size_t cols() const {
            return N;    
        }
        
        /*
         
        The matrix product operator. Let the FastTransformBase matrix be denoted 
        as `A`. Then this operator will perform the matrix product 
        `out = A*x` 
         
        INPUT:
         
        x    - The vector which will be multiplied with the matrix.
        out  - The vector the result of the matrix product will be written.
        sign - The scalar value in front of the matrix i.e., `out = sign*A*x`.  
        transpose - Whether or not the transpose of the matrix should be applied.
         
        */
        virtual void operator() ( T* x, T* out, T sign = 1.0,  const bool transpose=false) const = 0; 
        
    
};


/*

This class have been used for debugging purposes. It creates a random matrix, 
which is decomposed into a QR-factorization. The original random matrix and the 
R-matrix are discarded and only the Q-factor is stored. Any matrix product with
this class will be with this orthonormal Q-matrix. 


*/
template <typename T>
class RandQRMatrix : public FastTransformBase<T> {
    using FastTransformBase<T>::N;
    using FastTransformBase<T>::M;
     
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A;
     
    public: 
     
    RandQRMatrix(size_t M, size_t N) {
        this->M = M;
        this->N = N;
        typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> MatrixTd;
        // Create random matrix in the interval (-1,1)
        MatrixTd B = MatrixTd::Random(M,N);
         
         
        Eigen::HouseholderQR <MatrixTd> qr(B.transpose());
         
        MatrixTd Q = qr.householderQ();
        A = Q.leftCols(M);
        A.transposeInPlace();
         
    }
    
    
    friend std::ostream &operator<<( std::ostream &output, 
                                       const RandQRMatrix &ft_QR ) {
        output << ft_QR.A;
        return output;
    }

    void srand(unsigned int seed) {
        std::srand(seed);    
    }
    
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> getMatrix() {
        return A;    
    }
    
    void operator() ( T* x, T* out, T sign = 1.0,  const bool transpose=false) const;
    
    
};

/*

Simplest possible example of compressive sensing matrix. A 3 × 12 matrix which 
can solve a 1-sparse system. 

*/
template <typename T>
class CoinMatrix : public FastTransformBase<T> {
    using FastTransformBase<T>::N;
    using FastTransformBase<T>::M;
    
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A;
     
    public: 
     
    CoinMatrix() {
        
        M = 3;
        N = 12;
        Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> B(3,12);
        
        B << 1, 0, 0,-1, 0,-1, 1,-1, 0, 1,-1, 1,
             0, 1, 0,-1,-1, 0,-1, 0, 1, 1, 1,-1,
             0, 0, 1, 0,-1,-1, 0, 1,-1,-1, 1, 1;
        A = B;
    }
    
    
    friend std::ostream &operator<<( std::ostream &output, 
                                       const CoinMatrix &ft_CM ) {
        output << ft_CM.A;
        return output;
    }

    
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> getMatrix() {
        return A;    
    }
    
    void operator() ( T* x, T* out, T sign = 1.0,  const bool transpose=false) const;
    
    
};




/*

The matrix
            
                            P_Ω × HAD × IDWT
            
where P_Ω is the projection matrix onto the rows labeled in Ω. In this class 
this set Ω is called `idx`. `HAD` is the sequency ordered Hadamard matrix 
and `IDWT` is the inverse discrete wavelet transform.

*/
template <typename T>
class FastIDWT2WHT : public FastTransformBase<T> {
    using FastTransformBase<T>::N;
    using FastTransformBase<T>::M;
    
    
    Filter DB_g;
    Filter DB_h;
    size_t nu;
    const int vm;
    std::vector<int> idx;
    
    
    public: 
        // Get functions
        Filter get_DB_g() { 
            return DB_g;
        }
        
        Filter get_DB_h() { 
            return DB_h;
        }
        
        std::vector<int> get_idx() {
            return idx;
        }        
        
        size_t get_nu() {
            return nu;    
        }
        
        
        // Constructor 
        FastIDWT2WHT(const int vm, const size_t N,
                     const std::vector<int> &idx): vm(vm) {
            this->M = idx.size();
            this->idx = idx;
             
            DB_h = get_DB_filter(vm);
            DB_g = createG(DB_h);
             
            int N_upper = 1;
            nu = 0;
             
            while (N_upper < N) {
                N_upper *= 2;
                nu++;
            }
             
            if (N_upper > N) {
                std::cerr << "Warning: FastIDWT2WHT N = " << N << " is not a power"
                          << " of 2. Setting N to " << N_upper << std::endl;   
            }
             
            this->N = N_upper;
             
        }
        
        // Destructor and copy constructor
        ~FastIDWT2WHT() {
            // Not implemented
        }
        
        void operator() ( T* x, T* out, T sign = 1.0, const bool transpose=false) const;
    
};


template <typename T>
class FastIDWT2WHT2d : public FastTransformBase<T> {
    using FastTransformBase<T>::N;
    using FastTransformBase<T>::M;
    size_t m, n;
    unsigned int nres;
    Filter DB_g;
    Filter DB_h;
    size_t nu;
    const int vm;
    std::vector<int> idx;
    
    
    public: 
        // Get functions
        Filter get_DB_g() { 
            return DB_g;
        }
        
        Filter get_DB_h() { 
            return DB_h;
        }
        
        std::vector<int> get_idx() {
            return idx;
        }        
        
        size_t get_nu() {
            return nu;    
        }
        
        unsigned int get_nres() {
            return nres;    
        }
        
        // Constructor 
        FastIDWT2WHT2d(const int vm, const size_t N,
                     const std::vector<int> &idx): vm(vm) {
            this->M = idx.size();
            this->idx = idx;
             
            DB_h = get_DB_filter(vm);
            DB_g = createG(DB_h);
             
            int N_upper = 1;
            nu = 0;
             
            while (N_upper*N_upper < N) {
                N_upper *= 2;
                nu++;
            }
            nres = nu;
            m = N_upper;
            n = N_upper;
            
            while (N_upper < N) {
                N_upper *= 2;
                nu++;
            }
            
            if (N_upper > N) {
                std::cerr << "Warning: FastIDWT2WHT N = " << N << " is not a power"
                          << " of 2. Setting N to " << N_upper << std::endl;   
            }
             
            this->N = N_upper;
             
        }
        
        // Destructor and copy constructor
        ~FastIDWT2WHT2d() {
            // Not implemented
        }
        
        void operator() ( T* x, T* out, T sign = 1.0, const bool transpose=false) const;
    
};


template <typename T>
void createSparseVector(T *x, const size_t sparsity, const size_t N, bool debug=true);



