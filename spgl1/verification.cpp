/*

A test program, verifying that various parts of the code works. 

*/


#include "stdafx.h"
#include "spgl1Algorithm.h"
#include "supportImage.h"
bool test_Spgl1Algorithm_projectNorm1Real(const bool verbose=true);
bool test_Spgl1Algorithm_projectNorm1Complex(const bool verbose=true);
bool test_norm_and_dot_functions(const bool verbose=true);
bool test_LASSO_approx_residual(const bool verbose=true);
bool test_fastTransform_QR(const bool verbose=true);
bool test_Spgl1Algorithm_findMaxElementInVector(const bool verbose=true);
template <typename T>
bool test_fastTransform_geral_properties(FastTransformBase<T>& ft);
template <typename T>
bool test_fastTransform_unitary(FastTransformBase<std::complex<T> >& ft);
template <typename T>
bool test_fastTransform_unitary(FastTransformBase<T>& ft);
template <typename T>
bool test_fastTransform_signOperator(FastTransformBase<T> & ft);


bool test_FastIDWT2WHT(const bool verbose=true);

bool test_hadamard2d(const bool verbose=true);

int main(int argc, char *argv[]) {
    
    //test_norm_and_dot_functions();
    //bool test1 = test_Spgl1Algorithm_projectNorm1Real();
    //bool test2 = test_Spgl1Algorithm_projectNorm1Complex();
    //test_fastTransform_QR();
    //test_LASSO_approx_residual();
    //test_Spgl1Algorithm_findMaxElementInVector();
    //test_FastIDWT2WHT();
    test_hadamard2d(); 
    //test_IDWT2d();
    
    return 0;
}

bool test_hadamard2d(const bool verbose) {
     
    const unsigned int N = 16;
    const unsigned int m = 4;
    const unsigned int n = 4;
     
    double *x = new double[N];   
     
    for (int i = 0; i < N; i++) {
        x[i] = i+1;
    }
     
    hadamard2d<double>(x,m,n); 
     
    double y[N] = {136 ,-16 ,0 ,-8 ,-64 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,-32 ,0 ,0 ,0 };
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += std::abs(y[i]-x[i]);
    }
     
    bool success = sum < 1e-12;
     
    if (verbose) {
        const char * result_text = (success) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
        std::cout << result_text << ": hadamard2d"  << std::endl;    
    }
     
    delete [] x;
    return success;
     
}


bool test_FastIDWT2WHT(const bool verbose) {
    
    const int vm = 2; 
    const int N_length = 16;
    
    std::vector<int> idx1;
    std::vector<int> idx2;
    for (int i = 0; i < N_length; i++) {
        idx1.push_back(i);
    }


    FastIDWT2WHT<double> ft1(vm, N_length, idx1); 
     
    const size_t M = ft1.rows();
    const size_t N = ft1.cols();


    bool success1 = true; 
    if (M != 16 or N != 16) {
         success1 = false;   
    } 
     
    for (int i = 0; i < N_length/3; i++) {
        idx2.push_back(2*i);
    }
    
    FastIDWT2WHT<double> ft2( vm, N_length, idx2 ); 
    
    bool success2 = test_fastTransform_unitary<double>(ft1); 
    bool success3 = test_fastTransform_signOperator<double>(ft2); 
    
    const char * result_text1 = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    const char * result_text2 = (success2) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    const char * result_text3 = (success3) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
     
    if (verbose) {
        std::cout << result_text1 << ": FastIDWT2WHT row and cols"  << std::endl;    
        std::cout << result_text2 << ": FastIDWT2WHT unitary"  << std::endl;    
        std::cout << result_text3 << ": FastIDWT2WHT sign operator"  << std::endl;    
    }
     
     
     
     
        
}

template <typename T>
bool test_fastTransform_signOperator(FastTransformBase<T> & ft) {
      
    double eps = 1e-10;

    const size_t M = ft.rows();  
    const size_t N = ft.cols();  
      
    T x[N];
    T x_copy[N];

    T x_neg[N];
    T x_neg_copy[N];
    T result[N];   
     
    // ************************************************************************
    // ***       Test sign and control that x is left untouched             ***
    // ************************************************************************
     
    // Initialize x;
    for (int i = 0; i < N; i++) {
        x[i] = i-6;
    }
    x[0]=30; 
    x[N-1] = 4;  
     
    // x_neg = -x
    for (int i = 0; i < N; i++) {
        x_neg[i] = -x[i];
    }
     
    std::memcpy(x_copy, x, sizeof(T)*N);
    std::memcpy(x_neg_copy, x_neg, sizeof(T)*N);
     
    // result = -A*x;
    ft(x, result, -1, false);  
     
    double sum1 = 0;
    for (int i = 0; i < N; i++) {
        sum1 += std::abs(x[i] - x_copy[i]);
    }
    // x = 40; 
    std::memset(x,40, sizeof(T)*N);
     
    // x = A*x_neg
    ft(x_neg, x, 1, false);  
     
    double sum2 = 0;
    for (int i = 0; i < N; i++) {
        sum2 += std::abs(x_neg[i] - x_neg_copy[i]);
    }
     
     
    double sum3 = 0;
    for (int i = 0; i < M; i++) {
        sum3 += std::abs(result[i] - x[i]); 
    }
     
     
    // ************************************************************************
    // ***   Test sign of transpose and control that x is left untouched    ***
    // ************************************************************************
     
    // Initialize x;
    for (int i = 0; i < N; i++) {
        x[i] = i-6;
    }
    x[0]=30; 
    x[N-1] = 4;  
     
    // x_neg = -x
    for (int i = 0; i < N; i++) {
        x_neg[i] = -x[i];
    }
     
    std::memcpy(x_copy, x, sizeof(T)*N);
    std::memcpy(x_neg_copy, x_neg, sizeof(T)*N);
     
     
    // result = -A*x;
    ft(x, result, -1, true);  
     
    double sum4 = 0;
    for (int i = 0; i < N; i++) {
        sum4 += std::abs(x[i] - x_copy[i]);
    }
     
    // x = 40; 
    std::memset(x,40, sizeof(T)*N);
     
    // x = A*x_neg
    ft(x_neg, x, 1, true);  
     
    double sum5 = 0;
    for (int i = 0; i < N; i++) {
        sum5 += std::abs(x_neg[i] - x_neg_copy[i]);
    }
     
    double sum6 = 0;
    for (int i = 0; i < N; i++) {
        sum6 += std::abs(result[i] - x[i]); 
    }
     
    return (sum1 < eps and sum2 < eps and sum3 < eps and sum4 < eps and 
            sum5 < eps and sum6 < eps ); 
     
} 


/*

We are assuming ft is an 16 × 16 matrix, which is supposed to be unitary;  

*/
template <typename T>
bool test_fastTransform_unitary(FastTransformBase<T>& ft) {
     
    double eps = 1e-8; 
     
    const size_t M = ft.rows();
    const size_t N = ft.cols();
     
    if (M != 16 or N != 16) {
        std::cerr << "test_fastTransform_unitary assume ft is 16 × 16 matrix" << std::endl;    
        return false;
    } 
      
    T x[N]; 
    for (int i = 0; i < 16; i++) {
        x[i] = i+3;
    }
    x[2] = -30;
    x[6] = -5;
     
    T y[N];
    T result[N];
    std::memcpy(y,x,sizeof(T)*N);
     
    // result = A*x
    ft(x, result);
     
    // x = 50;
    std::memset(x,50, sizeof(T)*N);
     
    // x = A^T * result;
    ft(result, x, 1, true);
     
    // sum = ||x-y||_1
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += std::abs(x[i] - y[i]);
    }
     
    if (sum > eps) {
        return false;    
    } else {
        return true;    
    }
}

template <typename T>
bool test_fastTransform_unitary(FastTransformBase<std::complex<T> >& ft) {
     
    double eps = 1e-8; 
     
    size_t M = ft.rows();
    size_t N = ft.cols();
     
    if (M != 16 or N != 16) {
        std::cerr << "test_fastTransform_unitary assume ft is 16 × 16 matrix" << std::endl;    
        return false;
    } 
     
    std::complex<T> x[N]; 
    for (int i = 0; i < 16; i++) {
        x[i] = std::complex<T>(i+3, -i+2);
    }
    x[2] = std::complex<T>(-30, -7);
    x[6] = std::complex<T>(-5, 4);
     
    std::complex<T> y[N];
    std::complex<T> result[N];
    std::memcpy(y,x,sizeof(std::complex<T>)*N);
     
    // result = A*x
    ft(x, result);
     
    // x = 50;
    std::memset(x,50, sizeof(std::complex<T>)*N);
     
    // x = A^T * result;
    ft(result, x, 1, true);
     
    // sum = ||x-y||_1
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += std::abs(x[i] - y[i]);
    }
     
    if (sum > eps) {
        return false;    
    } else {
        return true;    
    }
}


bool test_fastTransform_QR(const bool verbose) {
     
    double eps = 1e-14;    
     
    size_t M = 4;
    size_t N = 4;
     
    RandQRMatrix<double> ft_QR1(M,N);
    ft_QR1.srand(5);
     
    // Test if unitary
    double x[N] = {1,2,3,4};
    double y[N];
    double result[N];
    std::memcpy(y,x,sizeof(double)*N);
     
    ft_QR1(x, result);
    ft_QR1(result, x, 1, true);
     
    double sum = 0;
    for (int i = 0; i < N; i++) {
        sum += std::abs(x[i] - y[i]);
    }
     
    bool success1 = true;
    if (sum > eps) {
        success1 = false;    
    }
     
    const char * result_text = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
     
    if (verbose) {
        std::cout << result_text << ": ft_QR unitary"  << std::endl;    
    } 
     
     
    // Test if sign works
     
    M = 3;
    RandQRMatrix<double> ft_QR2(M,N);
    ft_QR2.srand(5);
     
    double x_neg[N];
    std::memcpy(x,y,sizeof(double)*N);
    std::memcpy(x_neg,y,sizeof(double)*N);
     
    double result_neg[N]; 
    ft_QR2(x, result); 
    ft_QR2(x_neg, result_neg, -1, false); 
     
    sum = 0;
    for (int i = 0; i < M; i++) {
        sum += std::abs(result[i] + result_neg[i]);
        sum += std::abs(x_neg[i] - x[i]);
    }
     
    bool success2 = true;
    if (sum > eps) {
        success2 = false;    
    }
    
    result_text = (success2) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ft_QR sign"  << std::endl;    
    } 
     
    // Test if sign works for transpose
     
     
    std::memcpy(x,y,sizeof(double)*N);
    std::memcpy(x_neg,y,sizeof(double)*N);
     
    std::memset(result, 0, sizeof(double)*N);
    std::memset(result_neg, 0, sizeof(double)*N);

    ft_QR2(x, result,1, true); 
    ft_QR2(x_neg, result_neg, -1, true); 
     
    sum = 0;
    for (int i = 0; i < N; i++) {
        sum += std::abs(result[i] + result_neg[i]);
        sum += std::abs(x_neg[i] - x[i]);
    }
     
    bool success3 = true;
    if (sum > eps) {
        success3 = false;    
    }
    
    result_text = (success3) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ft_QR sign transpose"  << std::endl;    
    } 
     
    return (success1 and success2 and success3); 
     
}


/*

This test is under construction

*/
bool test_LASSO_approx_residual(const bool verbose) {
    
    const size_t N = 128;
    const size_t M = 50;
    const size_t sparsity = 14;
    RandQRMatrix<double> ft_QR(M,N);
     
    double b[M];
    double x0[N];
    createSparseVector<double>(x0, sparsity, N); 
    

    ft_QR(x0, b);
     
    Spgl1Algorithm spgl1(0.01, 9, 0.5, 100);
     
    double tau      = 0;
    double delta    = 0.5;
    double eps      = 1e-4;
    double *x_tau = new double[N];
    double *r_tau = new double[M];
     
    std::memset(x_tau,0, sizeof(double)*N);
    std::memset(r_tau,0, sizeof(double)*M);
     
    spgl1.LASSO_approx<double>(ft_QR, b, tau, delta, x_tau, r_tau);
    
    double x0_norm1 = norm1<double>(x0,N);
    double x_tau_norm1 = norm1<double>(x_tau, N);
    double r_tau_norm1 = norm1<double>(r_tau, M);
    double b_norm1 = norm1<double>(b, M);
    
    std::cout << "tau: " << tau << std::endl;
    std::cout << "||x_0||_1: " << x0_norm1 << std::endl;
    std::cout << "||x_τ||_1: " << x_tau_norm1 << std::endl;
    std::cout << "||r_τ||_1: " << r_tau_norm1 << std::endl;
    std::cout << "||b||_1: "   << b_norm1 << std::endl;
    
    
    
    
    
    
    
}



bool test_Spgl1Algorithm_findMaxElementInVector(const bool verbose) {
    
    double eps = 1e-15;
    const char * result_text;
    
    // INITIALIZATION
    Spgl1Algorithm alg(0,1,1, 8); // alpha_min, alpha_max, delta, linesearchHistoryLength
    
    std::vector<double> r;
    r.push_back(100);
    r.push_back(101);
    r.push_back(10);
    r.push_back(90);
    r.push_back(50);
    
    // Test 1 - Check all elements
    double r_max = alg.findMaxElementInVector(r, 30, 20);
    bool success1 = std::abs(101 - r_max) < eps;
    
    // Test 2 - Check all except the last element
    r_max = alg.findMaxElementInVector(r, 4, 5);
    success1 = success1 and std::abs(101 - r_max) < eps;
    
    // Test 3 - Check all except the last element, new max element
    r[0] = 102;
    r_max = alg.findMaxElementInVector(r, 4, 5);
    success1 = success1 and std::abs(101 - r_max) < eps;
    
    // Test 4 - Check all except the last element, new min
    r[0] = 102;
    r_max = alg.findMaxElementInVector(r, 30, 4);
    success1 = success1 and std::abs(101 - r_max) < eps;
    
    // Test 5 - Check all except the last element, new min
    r[0] = 102;
    r_max = alg.findMaxElementInVector(r, 3, 1);
    success1 = success1 and std::abs(50 - r_max) < eps;
    
    // I do not test what happens if the min argument is 0  
    
    result_text = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": findMaxElementInVector"  << std::endl;    
    } 
     
    return success1;
}






bool test_Spgl1Algorithm_projectNorm1Complex(const bool verbose) {
    
    // INITIALIZATION

    double eps = 1e-13;

    const int N = 5;
    
    std::complex<double> x[N];
    std::complex<double> y[N];
    x[0] = std::complex<double>(-200,100); 
    x[1] = std::complex<double>(0,0); 
    x[2] = std::complex<double>(3,-4); 
    x[3] = std::complex<double>(50,50); 
    x[4] = std::complex<double>(120,-50); 
    
    
    std::memcpy(y, x, sizeof(std::complex<double>)*N); 
    
    Spgl1Algorithm alg(0,1,1, 8); // alpha_min, alpha_max, delta, linesearchHistoryLength
    
    // TEST 1
    
    alg.projectNorm1<double>(x, N, 430);
    
    bool success1 = true;
    for (int i = 0; i < N; i++) {
        success1 = (success1  and std::abs(x[i] - y[i]) < eps);
    } 
    
    const char * result_text = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ProjectNorm1Complex τ ≥ ||x||_1"  << std::endl;    
    } 
     
    // TEST 2
    
    std::memcpy(x, y, sizeof(std::complex<double>)*N);
    
    bool success2 = true;
    bool phaseOK = true;
    
    double x_norm1 = norm1<std::complex<double> >(y,N);
    
    alg.projectNorm1<double>(x, N, x_norm1-4*5);
    
    for (int i = 0; i < N; i++) {
        phaseOK = std::abs(std::arg(x[i]) - std::arg(y[i])) < eps;
    }
    
    if ((not phaseOK) or 
        std::abs(y[0]*(std::abs(y[0])-5)/std::abs(y[0]) - x[0]) > eps or 
        std::abs(y[2]*(std::abs(y[2])-5)/std::abs(y[2]) - x[2]) > eps or 
        std::abs(y[3]*(std::abs(y[3])-5)/std::abs(y[3]) - x[3]) > eps or 
        std::abs(y[4]*(std::abs(y[4])-5)/std::abs(y[4]) - x[4]) > eps or
        std::abs(x[1]) > eps) {
         
        success2 = false;
         
    }
    
    result_text = (success2) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ProjectNorm1Complex τ = ||x||_1 -4*5"  << std::endl;    
    } 
    
    // TEST 3
    
    std::memcpy(x, y, sizeof(std::complex<double>)*N);
    
    bool success3 = true;
    phaseOK = true;
    
    x_norm1 = norm1<std::complex<double> >(y,N);
    
    alg.projectNorm1<double>(x, N, x_norm1-4*5 - 3*10);
    
    for (int i = 0; i < N; i++) {
        phaseOK = std::abs(std::arg(x[i]) - std::arg(y[i])) < eps;
    }
    
    if ((not phaseOK) or 
        std::abs(y[0]*(std::abs(y[0])-5-10)/std::abs(y[0]) - x[0]) > eps or 
        std::abs(y[3]*(std::abs(y[3])-5-10)/std::abs(y[3]) - x[3]) > eps or 
        std::abs(y[4]*(std::abs(y[4])-5-10)/std::abs(y[4]) - x[4]) > eps or
        std::abs(x[1]) > eps or
        std::abs(x[2]) > eps) {
         
        success3 = false;
         
    }
    
    result_text = (success3) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ProjectNorm1Complex τ = ||x||_1 -4*5 - 3*10"  << std::endl;    
    } 
    
    return success1 and success2 and success3;
    
}



bool test_Spgl1Algorithm_projectNorm1Real(const bool verbose) {
    

    double eps = 1e-14;

    const int N = 5;
    
    double x[N] = { -200, 50, 120, 0, -4 };  
    double y[N];
     
    std::memcpy(y, x, sizeof(double)*N); 
    
    Spgl1Algorithm alg(0,1,1, 8); // alpha_min, alpha_max, delta, linesearchHistoryLength
    
    // TEST 1
    
    alg.projectNorm1<double>(x, N, 200+50+120+4);
    
    bool success1 = true;
    for (int i = 0; i < N; i++) {
        success1 = (success1  and std::abs(x[i] - y[i]) < eps);
    } 
    
    const char * result_text = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ProjectNorm1Real τ ≥ ||x||_1"  << std::endl;    
    } 
     
    // TEST 2
    
    std::memcpy(x, y, sizeof(double)*N);
     
    bool success2 = true;
    alg.projectNorm1<double>(x, N, 370);
    
    if ( std::abs(x[0] + 199) > eps or std::abs(x[1] - 49) > eps or 
         std::abs(x[2] - 119) > eps or std::abs(x[3]) > eps or 
         std::abs(x[4] +   3) > eps ) {
         success2 = false;
    } 
    
    //std::cout << x[0] << ", " << x[1] << ", " << x[2] << ", " << x[3]  << ", " << x[4] << std::endl;
    
    result_text = (success2) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout << result_text << ": ProjectNorm1Real τ = 370"  << std::endl;    
    } 
     
    // TEST 3
    
    std::memcpy(x, y, sizeof(double)*N);
     
    bool success3 = true;
    alg.projectNorm1<double>(x, N, 358);
    
    if ( std::abs(x[0] + 196) > eps or std::abs(x[1] - 46) > eps or 
         std::abs(x[2] - 116) > eps or std::abs(x[3]) > eps or 
         std::abs(x[4]) > eps ) {
         success3 = false;
    } 
    
    //std::cout << x[0] << ", " << x[1] << ", " << x[2] << ", " << x[3]  << ", " << x[4] << std::endl;
    
    result_text = (success3) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";
    
    if (verbose) {
        std::cout  << result_text << ": ProjectNorm1Real τ = 358"<< std::endl;    
    } 
    //std::cout << x[0] << ", " << x[1] << ", " << x[2] << ", " << x[3]  
    //          << ", " << x[4] << std::endl;
     
     
    return (success1 and success2 and success3);
     
     
}


bool test_norm_and_dot_functions(const bool verbose) {
    
    double eps = 1e-14;
    
    // TEST OF SHORT SIGNAL
    double a = -7.5;
    double b = 9.0;
    std::complex<double> c = std::complex<double>(3,4);
    std::complex<double> d = std::complex<double>(3,7);
     
    size_t N = 1;
    double s1 = norm1<double>(&a, N);
    double s2 = norm2<double>(&a, N);
    double s3 = normInfty<double>(&a, N);
    double s4 = norm1<std::complex<double> >(&c, N);
    double s5 = norm2<std::complex<double> >(&c, N);
    double s6 = normInfty<std::complex<double> >(&c, N);
    
    double d1 = dot<double>(&a,&b,N);
    std::complex<double> d2 = dot<double >(&c,&d,N);
    
    
    
    bool success1 = true;
    
    if (std::abs(std::abs(a)-s1) > eps or std::abs(std::abs(a)-s2) > eps or std::abs(std::abs(a)-s3) > eps or 
        std::abs(5-s4) > eps or std::abs(5-s5) > eps or std::abs(5-s6) > eps or
        std::abs(d1 - a*b) > eps or std::abs(c*std::conj(d) - d2) > eps) {
        success1 = false;
    }
      
    const char * result_text = (success1) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";  
     
    if (verbose) {
        std::cout << result_text << ": Norm and dot of one number" << std::endl;
    } 
    
    // TEST OF 2-ITEM ARRAY
    N = 2;
    double x[N] = {3, -4};  
    double y[N] = {6, 2};
    
    std::complex<double> z[2];
    std::complex<double> w[2];
    
    z[0] = std::complex<double>(1,1);
    z[1] = std::complex<double>(3,-4);
    
    w[0] = std::complex<double>(1,1);
    w[1] = std::complex<double>(2,-2);
     
    s1 = norm1<double>(x, N);
    s2 = norm2<double>(x, N);
    s3 = normInfty<double>(x, N);
    s4 = norm1<std::complex<double> >(z, N);
    s5 = norm2<std::complex<double> >(z, N);
    s6 = normInfty<std::complex<double> >(z, N);
     
    d1 = dot<double>(x,y,N);
    d2 = dot<double>(z,w,N);
    
    bool success2 = true;
    
    if (std::abs(7-s1) > eps or std::abs(5-s2) > eps or std::abs(4-s3) > eps or 
        std::abs(5+std::sqrt(2)-s4) > eps or std::abs(std::sqrt(27)-s5) > eps 
        or std::abs(5-s6) > eps or std::abs(d1 - 10) > eps 
        or std::abs(std::complex<double>(16,-2)- d2) > eps) {
        success2 = false;
    }
      
    result_text = (success2) ? "\e[32mPassed\e[0m":"\e[31mFailed\e[0m";  
     
    if (verbose) {
        std::cout << result_text << ": Norm and dot of array"  << std::endl;
    } 
    
    return (success1 and success2);
    
}




