#include "supportImage.h"


/*

Copies the image into a vector where each column is placed on top of each other.
Remember to delete the vector after use.

It is assumed the image is a one channel unsigned char image.

*/
double * convertImageToVector(const cv::Mat & image ) {
     
    if (image.depth() != CV_8U) {
        std::cerr << "Wrong data type" << std::endl;
        return 0;
    }
     
    double *out = new double[image.rows*image.cols];
     
    int k = 0;
    for (int i = 0; i < image.rows; i++) {
        for (int j = 0; j < image.cols; j++) {
            out[k] = (double) image.at<unsigned char>(i,j);
            k++;
        }
    }    
        
    return out;
     
}

// x - row number, y - column number
const int convertTo1dIdx(const int x, const int y, const int im_cols) {
    return x*im_cols + y;
}

// id - one dim idx, 
void convertTo2dIdx(const int id, int& x, int& y, const int im_cols){
    x = id/im_cols;
    y = id%im_cols;
}

const std::vector<int> create_rect_random_subsampling(
                             const std::vector<int> & scheme) {
    
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
     
    const unsigned int nu = scheme.size();
    const unsigned int im_rows = powDyadic(nu+1);
    const unsigned int N = powDyadic(2*(nu+1));
    unsigned char *filled = new unsigned char[N];
    for (int i = 0; i < N; i++) {
        filled[i] = 0;
    }
    
    int sum = 0;
    for (int i = 0; i < nu; i++) {
        sum += scheme[i];
    }
    const unsigned int M = sum + 4;
    std::vector<int> idx (M,0);
    
     
     
    idx[0] = convertTo1dIdx(0,0, im_rows);     
    idx[1] = convertTo1dIdx(1,0, im_rows);     
    idx[2] = convertTo1dIdx(0,1, im_rows);     
    idx[3] = convertTo1dIdx(1,1, im_rows);     
     
    int cnt = 4;
    int k = 0;
    int s = powDyadic(k+1);
    s = 3*s*s;
     
    while(s == scheme[k]) {
         
        unsigned int ll = powDyadic(k+1);
        unsigned int lh = powDyadic(k+2);
    //    std::cout << "Filling "<< ll << ":" << lh << std::endl;
        for (int i = ll; i < lh; i++) {
             
            for (int j = 0; j < lh; j++) {
                idx[cnt] = convertTo1dIdx(i,j,im_rows);
                if (idx[cnt] >= N) {
                    std::cerr << "ERROR: converting indices wrong" << std::endl;    
                } else {
                    filled[idx[cnt]] = 1;
                }
                cnt++;
            }
             
            for (int j = 0; j < ll; j++) {
                idx[cnt] = convertTo1dIdx(j,i,im_rows);
                if (idx[cnt] >= N) {
                    std::cerr << "ERROR: converting indices wrong" << std::endl;    
                } else {
                    filled[idx[cnt]] = 1;
                }
                cnt++;
            }
        }
        
        k++;
        s = powDyadic(k+1);
        s = 3*s*s;
         
    }
    
    if (s-scheme[k] < 2000) {
        std::cerr << "Waring: To generate the random numbers might take some time"
                  << std::endl;
    }
     
    while (k < nu) {
        
        unsigned int ll = powDyadic(k+1);
        unsigned int lh = powDyadic(k+2);
        std::uniform_int_distribution<> dist(0, lh-1); 
        s = 0;
        while(s < scheme[k]) {
            int x = dist(generator);
            int y = dist(generator);
            if (x > ll or y > ll) {
                int t = convertTo1dIdx(x,y,im_rows);
                if (!filled[t]) {
                    filled[t] = 1;
                    idx.at(cnt) = t;
                    cnt++;
                    s += 1;
                }
            }
        }
        k++;
    }
     
    delete [] filled;
    return idx; 
}

std::vector<int> create_rectangular_scheme(const unsigned int M, 
                                           const unsigned int N) {
    
    unsigned int nu = 0;
    unsigned int im_rows = 1;
    while(im_rows*im_rows < N) {
        im_rows *= 2;
        nu++;
    }
    std::vector<int> scheme = std::vector<int>(nu-1,2);  
    
    int k = 1;
    unsigned int sum = 1;
    scheme[0] = 2;
    unsigned int s = 1;
    
    while (3*sum < M and k < 20) {
        
        s = powDyadic(k+1) - powDyadic(k);
        
        for (int i = k; i < nu-1; i++) {
            scheme[i] += s;
        }
        
        sum = 0;
        
        for (int i = 0; i < nu-1; i++) {
           sum += scheme[i]*scheme[i];
        }
        
        k++;
    
    } 
    k--;
     
    int sumSq = 0;
    for (int i = 0; i < k; i++) {
        sumSq += scheme[i]*scheme[i];
    } 
    
    sum = powDyadic(2*nu);
    
    while (3*(sum+sumSq) >= M) {
        sum = 0;
        for (int i = k; i < nu-1; i++) {
            scheme[i]--;
            sum += scheme[i]*scheme[i];
        }
    }
    
    for (int i = 0; i < nu-1; i++) {
        scheme[i] = 3*scheme[i]*scheme[i];
    }

    int upperBound = powDyadic(k+1); 
    upperBound = 3*upperBound*upperBound;
     
    sumSq = 0;
    for (int i = 0; i < k; i++) {
        sumSq += scheme[i];
    }
     
    sum = 0;
    int i = 0;
    while ( (sum+sumSq) < M-4 ) {
        sum = 0;
        i = 0;
        for (int i = k; i < nu-1; i++) {
            sum += scheme[i];
        }
        while (i < (nu-1-k)) {
            if (scheme[k+i] < upperBound and (sum+sumSq) < M-4) {
                scheme[k+i]++;    
                sum++;
            }
            i++;
        }
    }
    sum = 0;
    for (int i = 0; i < nu-1; i++) {
        sum += scheme[i];
    } 
     
    return scheme;
    
}




template<typename T> 
void hadamard2d(T* x, const int N , const int M) {
    if (M != N) {  
        std::cerr << "Error: hadamard2d wrong usage" << std::endl; 
    }
     
     
    T * y = new T[N]; // Intermediate array
    // For each row perform the Hadamard transform
    for (int k = 0; k < N; k++) {
        hadamardSequency<T>(&x[k*N], N);
    }

    // For each column apply the Hadamard transform
    for (int k = 0; k < N; k++) {
        // Copy out the right column
        for (int i = 0; i < N; i++) {
            y[i] = x[i*N+k];
        } 
         
        hadamardSequency<T>(y, N);
         
        // Copy the calculated elements back in the array
        for (int i = 0; i < N; i++) {
            x[i*N+k] = y[i];
        } 
    }
     
    delete [] y;
     
}
template void hadamard2d(double* x, const int N , const int M);



// N - Image columns
// M - Image rows
template<typename T> 
void IDWT2d(T* x, const int N , const int M, const Filter &DB_h, 
            const Filter &DB_g, const int nres) {
     
    if (M != N) { 
        std::cerr << "Error: hadamard2d wrong usage" << std::endl; 
    }
     
     
    T * y = new T[N]; // Intermediate array
    
    // For each row perform the IDWT
    for (int k = 0; k < N; k++) {
        IDWTPeriodic<T>(&x[k*N], N, DB_h, DB_g, nres);
    }
    
    // For each column apply the IDWT
    for (int k = 0; k < N; k++) {
        // Copy out the right column
        for (int i = 0; i < N; i++) {
            y[i] = x[i*N+k];
        } 
         
        IDWTPeriodic<T>(y, N, DB_h, DB_g, nres);
         
        // Copy the calculated elements back in the array
        for (int i = 0; i < N; i++) {
            x[i*N+k] = y[i];
        } 
    }
     
    delete [] y;
     
}
template void IDWT2d<>(double* x, const int N , const int M, const Filter &DB_h, 
            const Filter &DB_g, const int nres);


template<typename T> 
void DWT2d(T* x, const int N , const int M, const Filter &DB_h, 
            const Filter &DB_g, const int nres) {
     
    if (M != N) { 
        std::cerr << "Error: hadamard2d wrong usage" << std::endl; 
    }
     
     
    T * y = new T[N]; // Intermediate array
    // For each row perform the DWT 
    for (int k = 0; k < N; k++) {
        DWTPeriodic<T>(&x[k*N], N, DB_h, DB_g, nres);
    }
    
    // For each column apply the DWT 
    for (int k = 0; k < N; k++) {
        // Copy out the right column
        for (int i = 0; i < N; i++) {
            y[i] = x[i*N+k];
        } 
         
        DWTPeriodic<T>(y, N, DB_h, DB_g, nres);
         
        // Copy the calculated elements back in the array
        for (int i = 0; i < N; i++) {
            x[i*N+k] = y[i];
        } 
    }
     
    delete [] y;
     
}

template void DWT2d<>(double* x, const int N , const int M, const Filter &DB_h, 
                      const Filter &DB_g, const int nres);




/*

Based on a sampling scheme with values [m_1, ..., m_n] this function will 
sample m_k within each layer of size 2^k and return an index vector, with the 
chosen samples. 

*/
std::vector<int> create_uniform_random_subsampling(std::vector<int> & scheme) {
    
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
     
    const int n = scheme.size();
    scheme[n-1]--; // This is a poor solution, but is works.
    
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += scheme[i];
    }
    
    std::vector<int> idx (s,0);;
    
    int idx_cnt = 1;
    int K_itr = 1;
    for (int k = 1; k <= n; k++) {
        int lower = K_itr;
        K_itr *= 2;
        int upper = K_itr;
        //std::cout << "[ " << lower << ", " << upper << " ]" << std::endl;
        //std::cout << "area[k-1]: " << area[k-1] <<", samples[k]: " 
        //          << samples[k]  << std::endl;
        //std::cout << "upper-lower:" << upper-lower << std::endl;
        if ( (upper-lower) == scheme[k-1] ) {
            for (int i = lower; i < upper; i++) {
                idx[idx_cnt] = i;
                idx_cnt++;
            }
            
        } else {
            std::vector<int> tmp(upper-lower,0);
            int j = 0;
            for (int i = lower; i < upper; i++) {
                 
                tmp[j] = i;
                j++;
                 
            }
            
            shuffle(tmp.begin(), tmp.end(), std::default_random_engine(seed));
            
            for (int i = 0; i < scheme[k-1]; i++) {
                //std::cout << "tmp[" << std::setw(2) << i<< "]: " << tmp[i]  << std::endl;
                idx[idx_cnt] = tmp[i];
                idx_cnt++;
            }
        }
    }
    scheme[n-1]++;
     
    std::sort(idx.begin(), idx.end()); 
     
    return idx;
     
}






/*


For a multilevel sampling scheme the size of the different bins is 2^k. This 
function will try to distribute the size of the different bins as uniformly as 
possible. The first bins will necessarily not have room for a uniform distribution, 
so the extra samples from these bins will be distributed equally among the others.


*/
// NOTE: To make this function correct I need to rewrite it, so that the first 
// bin is equal to 2 and not 1. 
std::vector<int> create_uniform_sampling_scheme(const size_t M, const size_t N) {
    
    const int n = findMostSignificantBit(N)-1;
    
    if (N != powDyadic(n)) {
        std::cerr << "Error create_uniform_sampling_scheme: N is not a dyadic "
                  <<"power" << std::endl;    
    }
     
    // Calculate the maximum bin size
    std::vector<int>  bin_size(n,0);
    int pow_itr = 1;
     
    for (unsigned int i = 0; i < n; i++) {
        bin_size[i] = pow_itr;
        pow_itr *= 2;
    }
     
    int k = 0;
    int s = 0;
    pow_itr = 1;
    while ( s <= M ) {
        s = s + (n-k)*pow_itr;
        k = k + 1;
        pow_itr *= 2;
    }
     
    int diff = M - (powDyadic(k)-1);
    int E = diff/(n-k); // floor(diff/(n-k)) 
    std::cout << "diff: " << diff << ", E: " << E << std::endl;
     
    for (int i = k; i < n; i++) {
        bin_size[i] = E;
    }
     
    s = powDyadic(k)-1 + (n-k)*E;
    std::cout << "s: " << s << std::endl;
     
    int i = n-1;
    while(s < M) {
        bin_size[i] += 1;
        s += 1;
        i -= 1;
    }
     
    return bin_size;
     
}


/*

Scale the values of image into the range of [0,255] and converts it to an 
M×N image of unsigned character.   

*/
cv::Mat convertVectorToImage(double *image, const size_t M, const size_t N  ) {
    
    double minElem = std::numeric_limits<double>::max();
    double maxElem = std::numeric_limits<double>::min();
    
    for (int i = 0; i < M*N; i++) {
        minElem = minElem > image[i] ? image[i] : minElem;
        maxElem = maxElem < image[i] ? image[i] : maxElem;
    }
    
    std::cout << "minElem: " << minElem << std::endl;
    std::cout << "maxElem: " << maxElem << std::endl;
    
    for (int i = 0; i < M*N; i++) {
        image[i] = std::round(255*((image[i] - minElem)/(maxElem-minElem)));
    }
    
    cv::Mat cv_image(M,N, CV_8U);
    int k = 0;
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            cv_image.at<unsigned char>(i,j) = (unsigned char) image[k];
            k++;
        }
    }
    
    return cv_image;
    
}

/*
Creates a sampling pattern favouring the low coefficients, while applying random
subsampling of the others. The distribution of samples in each layer k is chosen 
according to the probabilities 

                            p_k = exp(-(bk/n)^a) 

For a thorough explanation of this formula, see "On asymptotic structure in
compressed sensing" by Roman, Adcock and Hansen eq. 8.

*/
std::vector<int> createSamplingPattern(const size_t N, const size_t M, 
                                       const double a, const double b) {
     
    std::vector<int> idx; 
    const int n = findMostSignificantBit(N)-1;
    const double m = ((double) M)/((double) N);
     
    if (n <= 6) {
        std::cerr << "createSamplingPattern may fail" << std::endl;    
    }
     
     
    double p_k[n];
    int area[n];
    unsigned int samples[n];
    unsigned int sum = 0;
     
    for (int k = 0; k < n; k++) {
         
        area[k] = powDyadic(k);
        double kernel = (b*k)/n;
         
        p_k[k] = std::exp(-std::pow(kernel, a));
        samples[k] = (unsigned int) std::ceil(p_k[k]*area[k]);
        sum += samples[k];
         
    }
     
    //std::cout << "sum: " << sum << ", M: 65536" << std::endl;
     
    int k = 0;
    while (sum < M) {
        if (samples[k] < area[k]) {
            samples[k] = samples[k]+1;
            sum += 1;
        }
        k++;
        k = k%n;
    }
     
    k = n-1;
    while (sum > M) {
        
        if (samples[k] > 0) {
            samples[k] = samples[k]-1;
            sum -= 1;
        }
        
        k  = (k <= 5) ? n-1: k-1;
    }
     
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
     
     
    idx.push_back(0);
    for (int k = 1; k <= n; k++) {
        int lower = powDyadic(k-1);
        int upper = powDyadic(k);
        //std::cout << "[ " << lower << ", " << upper << " ]" << std::endl;
        //std::cout << "area[k-1]: " << area[k-1] <<", samples[k]: " 
        //          << samples[k]  << std::endl;
        //std::cout << "upper-lower:" << upper-lower << std::endl;
        if ( (upper-lower) == samples[k-1] ) {
            for (int i = lower; i < upper; i++) {
                idx.push_back(i);
            }
            
        } else {
            
            std::vector<int> tmp;
            
            for (int i = lower; i < upper; i++) {
                tmp.push_back(i);
            }
            
            shuffle(tmp.begin(), tmp.end(), std::default_random_engine(seed));
            
            for (int i = 0; i < samples[k-1]; i++) {
                idx.push_back(tmp[i]);
            }
        }
    }
     
    std::sort(idx.begin(), idx.end()); 
     
    return idx;
     
}

void applyIDWT(double * image, const size_t N, const int vm) {
    
    const Filter DB_h = get_DB_filter(vm);
    const Filter DB_g = createG(DB_h);
    const int nres = findMostSignificantBit(N)-1;
     
    IDWTPeriodic<double>(image, N, DB_h, DB_g, nres);  
        
}




/*

Samples the image using an Hadamard matrix scaled by sqrt(N).

image - Image as vector
N     - Length of image
idx   - The indices one would like the function to return from this function
order - The order of the Hadamard matrix, i.e., ORDINARY, SEQUENCY or PALEY.

*/
double* hadamardSampleImage(double * image, const size_t  N, 
                            const std::vector<int> idx, HadamardOrder order) {
     
    const size_t M = idx.size();
    double *b = new double[M];
    double *x = new double[N];
    std::memcpy(x, image, sizeof(double)*N);
     
    if (order == ORDINARY) {
        hadamardOrdinary<double>(x,N);
    } else if (order == SEQUENCY) {
        hadamardSequency<double>(x,N);
    } else if (order == PALEY) {
        hadamardPaley<double>(x,N);    
    }
     
    for (int i = 0; i < M; i++) {
        b[i] = x[idx[i]]/std::sqrt((double) N);
    }
    
    delete [] x;

    return b;
     
}

/*

Samples the image using an Hadamard matrix scaled by sqrt(N).

image - Image as vector
N     - Length of image
idx   - The indices one would like the function to return from this function
order - The order of the Hadamard matrix, i.e., ORDINARY, SEQUENCY or PALEY.

*/
double* hadamardSampleImage2d(double * image, const size_t  N, 
                            const std::vector<int> idx) {
     
    const size_t M = idx.size();
    double *b = new double[M];
    double *x = new double[N];
    int im_rows = 1;
    while(im_rows*im_rows < N) {
        im_rows *= 2;    
    }
    std::cout << "im_rows: " << im_rows << std::endl;
    std::memcpy(x, image, sizeof(double)*N);
    hadamard2d<double>(x,im_rows, im_rows); 
     
    for (int i = 0; i < M; i++) {
        b[i] = x[idx[i]]/((double) im_rows);
    }
    
    delete [] x;

    return b;
     
}


/*
 
image            - Vector representation of the image.
N                - Length of the vector.
fractionSparisty ∈ [0,1] which amount of coefficients should we keep.
vm               - Number of vanishing moments.

returns a number in the interval [0,1] representing the fraction of how many 
entries that where neglected.
*/
double sparsifyImage(double *image, const size_t N, const double threshold , int vm) {
     
    const Filter DB_h = get_DB_filter(vm);
    const Filter DB_g = createG(DB_h);
    const int nres = findMostSignificantBit(N)-1;
     
    DWTPeriodic<double>(image, N, DB_h, DB_g, nres);  
     
    double image_norm1 = norm1<double>(image, N);
    double zeroCnt = 0;
     
    for (int i = 0; i < N; i++) {
        if (std::abs(image[i]) < threshold) {
            image[i] = 0;
            zeroCnt += 1;
        } 
    }
     
    IDWTPeriodic<double>(image, N, DB_h, DB_g, nres);  
     
    return zeroCnt/N; 
     
}


















