#include "supportFunctions.h"



template <typename T>
double norm1(const T * x, const size_t N) {
    
    double sum = 0;
    
    for (int i = 0; i < N; i++) {
        sum += std::abs(x[i]);
    }
    
    return sum;
    
}

template double norm1<>(const double * x, const size_t N);
template double norm1<>(const  std::complex<double> * x, const size_t N);

template <typename T>
double norm2 (const T *x, const size_t N) {
    
    double sum = 0;
    
    for (int i = 0; i < N; i++) {
        sum += std::abs(x[i]*x[i]);
    }
    
    return std::sqrt(sum);
    
}


template double norm2<>(const double *x, const size_t N);
template double norm2<>(const std::complex<double> *x, const size_t N);

template <typename T>
double normInfty(const T *x, const size_t N) {
    
    double max = -1;
    double value = 0;

    for (int i = 0; i < N; i++) {
        
        value = std::abs(x[i]);
        if (value > max) {
            max = value;    
        }
    }
    
    return std::abs(max);
   

}


template double normInfty<>(const double *x, const size_t N);
template double normInfty<>(const std::complex<double> *x, const size_t N);


template <typename T>
double dot(const T *x, const T *y, const size_t N) {
    
    T sum = 0;  
     
    for (int i = 0; i < N; i++) {
         
        sum += x[i]*y[i];
         
    } 
    
    return sum;
    
}


template <typename T>
std::complex<double> dot(const std::complex<T> *x, const std::complex<T> *y, const size_t N) {
     
    std::complex<double> sum = 0;  
     
    for (int i = 0; i < N; i++) {
         
        sum += x[i]*std::conj(y[i]);
         
    } 
    
    return sum;
    
}


template double dot<>(const double *x, const double *y, const size_t N);
template std::complex<double> dot<>(const std::complex<double> *x, const std::complex<double> *y, const size_t N);



