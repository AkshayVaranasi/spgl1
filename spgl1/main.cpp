/*

This function is currently not in use, but it has been used in the debugging of
the algorithm.

*/


#include "stdafx.h"
#include "spgl1Algorithm.h"






template <typename T>
void compareSolution(T* x0, T*x1, size_t N, double eps=0.2);


template <typename T>
double computeResidual(FastTransformBase<T>& ft, T* x, T* b);





int main(int argc, char *argv[]) {
   
    std::srand(10);
    bool debug = true;

    const size_t N = 128;
    const size_t M = 50;
    const size_t sparsity = 14;
    RandQRMatrix<double> ft_QR(M,N);
    
    double b[M];
    double x0[N];
    createSparseVector<double>(x0, sparsity, N, debug); 
    
    //for (int i = 0; i < N; i++) {
    //    std::cout << "x[" << std::setw(2) << i << "]: " << x0[i] << std::endl;
    //}
    
    ft_QR(x0, b);
     
     
    AlgorithmicConstants AC;
    AC.readConstants("config.txt");
    AC.printConstants(); 
    
    double alphaMin = AC.getAlphaMin();
    double alphaMax = AC.getAlphaMax();
    double delta    = AC.getDelta();
    double gamma    = AC.getGamma();
    double sigma    = AC.getSigma();
    int LASSOMaxItr = AC.getLASSOMaxItr();
    int lineSearchHistoryLength = AC.getLineSearchHistoryLength();
     
    Spgl1Algorithm spgl1(alphaMin, alphaMax, gamma,lineSearchHistoryLength);
     
    double tau      = AC.getTau();
    double eps      = 1e-4;
    double *x_tau = new double[N];
    double *r_tau = new double[M];
     
    std::memset(x_tau, 0, sizeof(double)*N);
    std::memset(r_tau, 0, sizeof(double)*M);
     
    double x0_norm1 = norm1<double>(x0,N);
    double x0_norm2 = norm2<double>(x0,N);
    double x_tau_norm1 = norm1<double>(x_tau, N);
    double r_tau_norm1 = norm1<double>(r_tau, M);
    double b_norm1 = norm1<double>(b, M);
    double b_norm2 = norm2<double>(b, M);
     
    //std::cout << "tau: " << tau << std::endl;
    std::cout << "||x_0||_1: " << x0_norm1 << std::endl;
    std::cout << "||x_0||_2: " << x0_norm2 << std::endl;
    //std::cout << "||x_τ||_1: " << x_tau_norm1 << std::endl;
    //std::cout << "||r_τ||_1: " << r_tau_norm1 << std::endl;
    //std::cout << "||b||_1: "   << b_norm1 << std::endl;
    //std::cout << "||b||_2: "   << b_norm2 << std::endl;
     
    //spgl1.LASSO_approx<double>(ft_QR, b, tau, delta, x_tau, r_tau);
    double *x1 = spgl1.rootFinding(ft_QR, b, sigma, LASSOMaxItr); 
    compareSolution(x0,x1,N);
    
    double residualOfSolution = computeResidual<double>(ft_QR, x1, b);
    
    std::cout << "||Ax - b||_2: " << residualOfSolution << std::endl;
    
    // Calculate Δτ
    double r_norm2 = norm2<double>(r_tau, M);  
    
    //std::cout << "||r||_2: " << r_norm2 << std::endl;
    
    for (int i = 0; i < M; i++) {
        r_tau[i] = r_tau[i]/r_norm2;  
    }
     
    double result[N];
    // ||A^T (r/||r||_2) ||_∞
    ft_QR(r_tau, result, 1.0, true); 
      
    double lambda_tau = normInfty<double>(result, N); 
    
    //std::cout << "||A^T (r/||r||_2) ||_∞: " << lambda_tau << std::endl;
    
    double delta_tau = (sigma - r_norm2)/(-lambda_tau);
     
    //std::cout << "\e[31mΔτ: " <<  delta_tau << "\e[0m" << std::endl;
     
    //for (int i = 0; i < N; i++) {
    //     
    //    if ( std::abs(x0[i]) > eps or std::abs(x_tau[i] > eps)) {
    //        std::cout << std::setw(3) << i << ": " << std::setw(10) << x0[i] 
    //                  << ", " << x_tau[i] << std::endl;        
    //    }
    //     
    //}
     
    //std::cout << "||r_tau||_2: " << norm2<double>(r_tau, M) << std::endl;
    //std::cout << "||x_tau||_2: " << norm2<double>(x_tau, N) << std::endl;
     
}



template <typename T>
void compareSolution(T* x0, T*x1, size_t N, double eps) {
    

    for (unsigned int i = 0; i < N; i++) {
         
        if ( std::abs(x0[i]) > eps or std::abs(x1[i]) > eps ) {
             
            std::cout << "i: " << std::setw(3) << i << ", "<< std::setw(14) 
                      << x0[i] << ", " << std::setw(14) << x1[i] << std::endl; 
             
             
        }
         
         
    } 
     
}

template <typename T>
double computeResidual(FastTransformBase<T>& ft, T* x, T* b) {
     
    const size_t N = ft.cols();
    const size_t M = ft.rows();
     
    T residual[M];
    ft(x,residual);
    for (int i = 0; i < M; i++) {
        residual[i] = residual[i] - b[i];
    }
    
     
    return norm2<T>(residual, M); 
     
}  











