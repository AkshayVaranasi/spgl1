#include "filter.h"

void Filter::push_back(double element){
    h.push_back(element);    
}

void Filter:: set_zero_idx(int idx) {
    zero_idx = idx;
    
}

int Filter::get_zero_idx() const {
    return zero_idx;    
}

int Filter::size() const{
    return h.size();
}

int Filter::start_idx() const{
    return -zero_idx;
}

int Filter::end_idx() const{
    
    return h.size()-zero_idx - 1;    
}

double Filter::operator[](int i) const {
    const int N = h.size();
    
    if (i >= (N-zero_idx) || i + zero_idx < 0 ) {
        return 0;    
    } else {
        return h[i + zero_idx];
    }
}



std::ostream & operator<<(std::ostream & os, Filter & f) {
    const int N = f.size();
    for (int i = f.start_idx(); i <= f.end_idx(); i++) {
        os << std::setw(3)<< i << ": "<< std::setprecision(12) << f[i] << std::endl;
    } 
    return os;
}


void Filter::printOrder() const{
    std::vector<double> x = h;
    const int N = h.size();
    std::vector<bool> legal(N, true);
    std::vector<int> idx_order(N,0);
    int i, j;
    for (i = 0; i < N; i++) {
        double min_elem = std::numeric_limits<double>::max();
        int min_idx = -1;
        for (j = 0; j < N; j++) {
            if ((std::fabs(x[j])) < min_elem && legal[j]) {
                min_elem = std::fabs(x[j]);
                min_idx = j;
            }
        }
        idx_order[min_idx] = i;
        legal[min_idx] = false;
    }
    
    
    std::vector<int>::iterator it = idx_order.begin();
    int k = start_idx();
    for (int i = 0; i < N; i++) {
        std::cout << std::setw(3)<< k << ": " <<std::setw(3) << *it << ": "<< std::setprecision(12) << h[i] << std::endl;
        it++; k++;
    } 
    
    
}

Filter createG(const Filter & f) {
    
    const int N = f.size();
    Filter g;

    int pm1; // pluss or minus 1
    for (int n = f.end_idx(); n >= f.start_idx(); n--) {
        
        if ((n)%2) { // Is odd
            pm1 = -1;
        } else {
            pm1 = 1;    
        }
        
        g.push_back(pm1*f[n]);
        
    }
    
    g.set_zero_idx(N-2 - f.get_zero_idx());
    return g;
}

Filter reverseFilter(Filter &f) {
    Filter f_rev;
    for (int n = f.end_idx(); n >= f.start_idx(); n--) {
        f_rev.push_back(f[n]);
    }
    const int N = f.size();
    const int zero_idx_f = f.get_zero_idx();
    
    f_rev.set_zero_idx(N-1 -zero_idx_f);
    return f_rev;
}

Filter get_DB_filter(int vm) {
    switch (vm) {
        
        case 2:
            return get_DB2();
        
        case 3:
            return get_DB3();
        
        case 4:
            return get_DB4();
        
        case 5:
            return get_DB5();
        
        case 6:
            return get_DB6();
        
        case 7:
            return get_DB7();
        
        case 8:
            return get_DB8();
        
        case 9:
            return get_DB9();
        
        case 10:
            return get_DB10();
        
        default:
            std::cerr << "Error: get_DB_filter(" << vm 
                      << ") is only supported for 2 <= vm <= 7" << std::endl;
            return get_DB2();
            
    }
        
}


Filter get_dyd4() {
     
    Filter dyd4;
     
    //  0 < a < 1
    // |a|^2 + |b|^2 = 1
     
    double a = 0.8660254;
    double b = -std::sqrt(1 - a*a);
     
    dyd4.push_back((1 + a + b)/(2.0*std::sqrt(2)));
    dyd4.push_back((1 + a - b)/(2.0*std::sqrt(2)));
    dyd4.push_back((1 - a - b)/(2.0*std::sqrt(2)));
    dyd4.push_back((1 - a + b)/(2.0*std::sqrt(2)));
     
    return dyd4;
     
}



Filter get_DB2() {
    Filter DB2_h;
    DB2_h.push_back( 0.482962913145);
    DB2_h.push_back( 0.836516303738);
    DB2_h.push_back( 0.224143868042);
    DB2_h.push_back(-0.129409522551);
    DB2_h.set_zero_idx(0);
    return DB2_h;
}








Filter get_DB3() {
    Filter DB3_h;
    DB3_h.push_back( 3.326705529500826159985115891390056300129233992450683597084705e-01);// 0.332670552950
    DB3_h.push_back( 8.068915093110925764944936040887134905192973949948236181650920e-01);// 0.806891509311
    DB3_h.push_back( 4.598775021184915700951519421476167208081101774314923066433867e-01);// 0.459877502118
    DB3_h.push_back(-1.350110200102545886963899066993744805622198452237811919756862e-01);//-0.135011020010
    DB3_h.push_back(-8.544127388202666169281916918177331153619763898808662976351748e-02);//-0.085441273882
    DB3_h.push_back( 3.522629188570953660274066471551002932775838791743161039893406e-02);// 0.035226291882
    DB3_h.set_zero_idx(0);
    return DB3_h;
}

Filter get_DB4() {
    Filter DB4_h;
    DB4_h.push_back( 0.230377813309);
    DB4_h.push_back( 0.714846570553);
    DB4_h.push_back( 0.630880767930);
    DB4_h.push_back(-0.027983769417);
    DB4_h.push_back(-0.187034811719);
    DB4_h.push_back( 0.030841381836);
    DB4_h.push_back( 0.032883011667);
    DB4_h.push_back(-0.010597401785);
    DB4_h.set_zero_idx(0);
    return DB4_h;
}

Filter get_DB5() {
    Filter DB5_h;
    DB5_h.push_back( 0.160102397974); // 0
    DB5_h.push_back( 0.603829269797); // 1
    DB5_h.push_back( 0.724308528438); // 2
    DB5_h.push_back( 0.138428145901); // 3
    DB5_h.push_back(-0.242294887066); // 4
    DB5_h.push_back(-0.032244869585); // 5
    DB5_h.push_back( 0.077571493840); // 6
    DB5_h.push_back(-0.006241490213); // 7
    DB5_h.push_back(-0.012580751999); // 8
    DB5_h.push_back( 0.003335725285); // 9
    DB5_h.set_zero_idx(0);
    return DB5_h;
}

Filter get_DB6() {
    Filter DB6_h;
    DB6_h.push_back( 0.111540743350); // 0
    DB6_h.push_back( 0.494623890398); // 1
    DB6_h.push_back( 0.751133908021); // 2
    DB6_h.push_back( 0.315250351709); // 3
    DB6_h.push_back(-0.226264693965); // 4
    DB6_h.push_back(-0.129766867567); // 5
    DB6_h.push_back( 0.097501605587); // 6
    DB6_h.push_back( 0.027522865530); // 7
    DB6_h.push_back(-0.031582039317); // 8
    DB6_h.push_back( 0.000553842201); // 9
    DB6_h.push_back( 0.004777257511); // 10
    DB6_h.push_back(-0.001077301085); // 11
    DB6_h.set_zero_idx(0);
    return DB6_h;
}


Filter get_DB7() {
    Filter DB7_h;
    DB7_h.push_back( 0.077852054085 ); // 0
    DB7_h.push_back( 0.396539319482 ); // 1
    DB7_h.push_back( 0.729132090846 ); // 2
    DB7_h.push_back( 0.469782287405 ); // 3
    DB7_h.push_back(-0.143906003929 ); // 4
    DB7_h.push_back(-0.224036184994 ); // 5
    DB7_h.push_back( 0.071309219267 ); // 6
    DB7_h.push_back( 0.080612609151 ); // 7
    DB7_h.push_back(-0.038029936935 ); // 8
    DB7_h.push_back(-0.016574541631 ); // 9
    DB7_h.push_back( 0.012550998556 ); // 10
    DB7_h.push_back( 0.000429577973 ); // 11
    DB7_h.push_back(-0.001801640704 ); // 12
    DB7_h.push_back( 0.000353713800 ); // 13
    DB7_h.set_zero_idx(0);
    return DB7_h;
}


Filter get_DB8() {
    
    Filter DB8_h;
    
    DB8_h.push_back( 5.441584224310400995500940520299935503599554294733050397729280e-02);
    DB8_h.push_back( 3.128715909142999706591623755057177219497319740370229185698712e-01);
    DB8_h.push_back( 6.756307362972898068078007670471831499869115906336364227766759e-01);
    DB8_h.push_back( 5.853546836542067127712655200450981944303266678053369055707175e-01);
    DB8_h.push_back(-1.582910525634930566738054787646630415774471154502826559735335e-02);
    DB8_h.push_back(-2.840155429615469265162031323741647324684350124871451793599204e-01);
    DB8_h.push_back( 4.724845739132827703605900098258949861948011288770074644084096e-04);
    DB8_h.push_back( 1.287474266204784588570292875097083843022601575556488795577000e-01);
    DB8_h.push_back(-1.736930100180754616961614886809598311413086529488394316977315e-02);
    DB8_h.push_back(-4.408825393079475150676372323896350189751839190110996472750391e-02);
    DB8_h.push_back( 1.398102791739828164872293057263345144239559532934347169146368e-02);
    DB8_h.push_back( 8.746094047405776716382743246475640180402147081140676742686747e-03);
    DB8_h.push_back(-4.870352993451574310422181557109824016634978512157003764736208e-03);
    DB8_h.push_back(-3.917403733769470462980803573237762675229350073890493724492694e-04);
    DB8_h.push_back( 6.754494064505693663695475738792991218489630013558432103617077e-04);
    DB8_h.push_back(-1.174767841247695337306282316988909444086693950311503927620013e-04);
    DB8_h.set_zero_idx(0);
    return DB8_h;
}


Filter get_DB9() {
    
    Filter DB9_h;
    
    DB9_h.push_back( 3.807794736387834658869765887955118448771714496278417476647192e-02);
    DB9_h.push_back( 2.438346746125903537320415816492844155263611085609231361429088e-01);
    DB9_h.push_back( 6.048231236901111119030768674342361708959562711896117565333713e-01);
    DB9_h.push_back( 6.572880780513005380782126390451732140305858669245918854436034e-01);
    DB9_h.push_back( 1.331973858250075761909549458997955536921780768433661136154346e-01);
    DB9_h.push_back(-2.932737832791749088064031952421987310438961628589906825725112e-01);
    DB9_h.push_back(-9.684078322297646051350813353769660224825458104599099679471267e-02);
    DB9_h.push_back( 1.485407493381063801350727175060423024791258577280603060771649e-01);
    DB9_h.push_back( 3.072568147933337921231740072037882714105805024670744781503060e-02);
    DB9_h.push_back(-6.763282906132997367564227482971901592578790871353739900748331e-02);
    DB9_h.push_back( 2.509471148314519575871897499885543315176271993709633321834164e-04);
    DB9_h.push_back( 2.236166212367909720537378270269095241855646688308853754721816e-02);
    DB9_h.push_back(-4.723204757751397277925707848242465405729514912627938018758526e-03);
    DB9_h.push_back(-4.281503682463429834496795002314531876481181811463288374860455e-03);
    DB9_h.push_back( 1.847646883056226476619129491125677051121081359600318160732515e-03);
    DB9_h.push_back( 2.303857635231959672052163928245421692940662052463711972260006e-04);
    DB9_h.push_back(-2.519631889427101369749886842878606607282181543478028214134265e-04);
    DB9_h.push_back( 3.934732031627159948068988306589150707782477055517013507359938e-05);
    
    DB9_h.set_zero_idx(0);
    return DB9_h;

}


Filter get_DB10() {
     
    Filter DB10_h;
     
    DB10_h.push_back( 2.667005790055555358661744877130858277192498290851289932779975e-02);
    DB10_h.push_back( 1.881768000776914890208929736790939942702546758640393484348595e-01);
    DB10_h.push_back( 5.272011889317255864817448279595081924981402680840223445318549e-01);
    DB10_h.push_back( 6.884590394536035657418717825492358539771364042407339537279681e-01);
    DB10_h.push_back( 2.811723436605774607487269984455892876243888859026150413831543e-01);
    DB10_h.push_back(-2.498464243273153794161018979207791000564669737132073715013121e-01);
    DB10_h.push_back(-1.959462743773770435042992543190981318766776476382778474396781e-01);
    DB10_h.push_back( 1.273693403357932600826772332014009770786177480422245995563097e-01);
    DB10_h.push_back( 9.305736460357235116035228983545273226942917998946925868063974e-02);
    DB10_h.push_back(-7.139414716639708714533609307605064767292611983702150917523756e-02);
    DB10_h.push_back(-2.945753682187581285828323760141839199388200516064948779769654e-02);
    DB10_h.push_back( 3.321267405934100173976365318215912897978337413267096043323351e-02);
    DB10_h.push_back( 3.606553566956169655423291417133403299517350518618994762730612e-03);
    DB10_h.push_back(-1.073317548333057504431811410651364448111548781143923213370333e-02);
    DB10_h.push_back( 1.395351747052901165789318447957707567660542855688552426721117e-03);
    DB10_h.push_back( 1.992405295185056117158742242640643211762555365514105280067936e-03);
    DB10_h.push_back(-6.858566949597116265613709819265714196625043336786920516211903e-04);
    DB10_h.push_back(-1.164668551292854509514809710258991891527461854347597362819235e-04);
    DB10_h.push_back( 9.358867032006959133405013034222854399688456215297276443521873e-05);
    DB10_h.push_back(-1.326420289452124481243667531226683305749240960605829756400674e-05);
     
    DB10_h.set_zero_idx(0);
    return DB10_h;
     
}





















