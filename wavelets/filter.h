#include <iostream>
#include <iomanip>
#include <vector>
#include <list>
#include <limits>
#include <cstdio>
#include <cmath>

# pragma once
class Filter {
    public:
    std::vector<double> h;
    int zero_idx;
    
    Filter(): zero_idx(0) {}
    
    void push_back(double element);
    void set_zero_idx(int idx);
    int get_zero_idx() const;
    int size() const;
    int start_idx() const;
    int end_idx() const;
    void printOrder() const;
    double operator[](int i) const; 
    friend std::ostream & operator<<(std::ostream & os, Filter & f);
    
};





Filter reverseFilter(Filter &f);
Filter createG(const Filter & f);

Filter get_dyd4();
Filter get_DB_filter(int vm);
Filter get_DB2();
Filter get_DB3();
Filter get_DB4();
Filter get_DB5();
Filter get_DB6();
Filter get_DB7();
Filter get_DB8();
Filter get_DB9();
Filter get_DB10();






