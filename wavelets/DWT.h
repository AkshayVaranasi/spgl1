#include <vector>
#include <iostream>
#include <cstring>
#include <cassert>
#include "filter.h"
#pragma once



template <typename T>
void DWTPeriodicOneRes(T * x, const size_t N, const Filter &DB_h, 
                                              const Filter &DB_g);

template <typename T>
void IDWTPeriodicOneRes(T * x, const size_t N, const Filter & DB_h, 
                                               const Filter &DB_g);

template <typename T>
int DWTPeriodic(T * x, const size_t N, const Filter &DB_h, const Filter &DB_g, 
                const int nres);

template <typename T>
int IDWTPeriodic(T * x, const size_t N, const Filter &DB_h, const Filter &DB_g, 
                 const int nres);


int mapToIntervalPeriodic(const int idx, const int a, const int b);


std::vector<double> DWTPeriodic(std::vector<double> & x, const Filter &h, 
                                const Filter &g, int nres);
std::vector<double> IDWTPeriodic(std::vector<double> & x, const Filter & h, 
                                 const Filter &g, int nres);
std::vector<double> DWTPeriodicOneRes(std::vector<double> & x, const Filter &h, 
                                      const Filter &g);
std::vector<double> IDWTPeriodicOneRes(std::vector<double> & x, const Filter & h, 
                                       const Filter &g);

