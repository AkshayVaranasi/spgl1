#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <list>
#include <cstring>
#include <cmath>
#include <stdio.h>
#include <complex>
#include <cassert>
#include "DWT.h"
#include "../fastwht/hadamard.h"
#include "Eigen/Dense"


void printVector(std::vector<double> v) {
    for (int i = 0; i < v.size(); i++) {
        std::cout << v[i] << std::endl;    
    }    
}

void printVector(std::vector<double> a, std::vector<double> b) {
    for (int i = 0; i < a.size(); i++) {
        std::cout << std::setw(3) << a[i] << " : " << std::setprecision(10) << b[i] << std::endl;    
    }    
}

template <typename T>
void diffVectors(std::vector<T> a, std::vector<T> b, const int N) {
    
    for (int i = 0; i < N; i++) {
        printf("%14.12f\n", a[i]-b[i]);
    }

}

void insertVector(Eigen::MatrixXd & A, std::vector<double> & v , const int col ) {
    
    const int N = A.rows();

    for (int i = 0; i < N; i++) {
        A(i,col) = v[i];
    }
    
}
std::vector<double> getVector(Eigen::MatrixXd & A, const int col) {
    
    std::vector<double> v;
    const int N = A.rows();
    for (int i = 0; i < N; i++) {
        v.push_back(A(i,col));
    }
    
    return v;
    
}


int main(int argc, char *argv[]) {
    
    const unsigned int nres = 5;
    const unsigned int vm = 2;

    const unsigned int N = 32;
	
	Filter DB_h = get_DB_filter(vm);
    Filter DB_g = createG(DB_h);
    Filter g = DB_g;
    Filter h = DB_h;
    Eigen::MatrixXd I = Eigen::MatrixXd::Identity(N,N);
    Eigen::MatrixXd U = Eigen::MatrixXd::Zero(N,N);
    Eigen::MatrixXd U_inv = Eigen::MatrixXd::Zero(N,N);
    
    for (int i = 0; i < N; i++) {
        std::vector<double> x = getVector(I, i);
        std::vector<double> x_dwt = DWTPeriodic(x, DB_h, DB_g, nres);
        std::vector<double> x_idwt = IDWTPeriodic(x, DB_h, DB_g, nres);
        insertVector(U, x_dwt, i);
        insertVector(U_inv, x_idwt, i);
    }
	
    std::cout << U_inv.leftCols<16>() << std::endl;
    
    double h1 = g[-1]*h[0];
    std::cout << h1 << std::endl;

    for (int vm = 2; vm < 11; vm++) {
	    Filter DB_h = get_DB_filter(vm);
        Filter DB_g = createG(DB_h);
        double s_h = 0;
        double s_g = 0;
        for (int i = -2*vm; i < 2*vm; i++) {
            s_h += std::abs(DB_h[i]);
            s_g += std::abs(DB_g[i]);
        }
         
        std::cout << "vm: " << vm << ", s_h: " << std::setw(10) << s_h 
                  << ", s_g: " << std::setw(10) << s_g << std::endl; 
         
    }
    
	//double *x = new double[N];
	//std::memset(x,0, sizeof(double)*N);
	//x[N-5] = 1;
	//IDWTPeriodic<double>(x,N, DB_h, DB_g, dim);
	//for (unsigned int i = 0; i < N; i++) {
	//    std::cout << x[i] << std::endl;
	//}
}

