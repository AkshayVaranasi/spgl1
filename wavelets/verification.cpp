#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <list>
#include <cstring>
#include <cmath>
#include <stdio.h>
#include <complex>
#include <cassert>
#include "DWT.h"
#include "../fastwht/hadamard.h"
#include "Eigen/Dense"


void printVector(std::vector<double> v) {
    for (int i = 0; i < v.size(); i++) {
        std::cout << v[i] << std::endl;    
    }    
}

void printVector(std::vector<double> a, std::vector<double> b) {
    for (int i = 0; i < a.size(); i++) {
        std::cout << std::setw(3) << a[i] << " : " << std::setprecision(10) << b[i] << std::endl;    
    }    
}

template <typename T>
void diffVectors(std::vector<T> a, std::vector<T> b, const int N) {
    
    for (int i = 0; i < N; i++) {
        printf("%14.12f\n", a[i]-b[i]);
    }

}


void test_one_res_signal_template(std::vector<double> &x, double *y, double eps);
void test_signal_template(std::vector<double> &x, double *y, int nres, double eps);

void test_one_res_signal(std::vector<double> & x, double eps) ;
void test_signal(std::vector<double> & x, double eps);
void printAllGfilters();
void printAllHfilters();

std::vector<double> get_testsignal1(); 
std::vector<double> get_testsignal2();
std::vector<double> get_testsignal3();

void test_one_res_signal1();
void test_one_res_signal2();

void test_one_res_template();
void test_signal_template();

void test_signal1();
void test_signal2();
void test_for_unitary(int vm, bool verbose=true);


std::vector<double> getVector(Eigen::MatrixXd & A, const int col) {
    
    std::vector<double> v;
    const int N = A.rows();
    for (int i = 0; i < N; i++) {
        v.push_back(A(i,col));
    }
    
    return v;
    
}

void insertVector(Eigen::MatrixXd & A, std::vector<double> & v , const int col ) {
    
    const int N = A.rows();

    for (int i = 0; i < N; i++) {
        A(i,col) = v[i];
    }
    
}



int main(int argc, char *argv[]) {
        
        //test_one_res_signal1();
        //test_one_res_signal2();
        //test_one_res_template();
        //
        //test_signal1();
        //test_signal2();
        //test_signal_template();
        test_for_unitary(2);  
}


void test_for_unitary(int vm, bool verbose ) {
    
    Filter DB_h = get_DB_filter(vm);
    Filter DB_g = createG(DB_h);
    
    const int N = 16;
    const double eps = 1e-12;
    
    Eigen::MatrixXd I = Eigen::MatrixXd::Identity(N,N);
    
    Eigen::MatrixXd U = Eigen::MatrixXd::Zero(N,N);
    Eigen::MatrixXd U_inv = Eigen::MatrixXd::Zero(N,N);
    
    for (int i = 0; i < N; i++) {
        std::vector<double> x = getVector(I, i);
        std::vector<double> x_dwt = DWTPeriodic(x, DB_h, DB_g, 4);
        std::vector<double> x_idwt = IDWTPeriodic(x, DB_h, DB_g, 4);
        insertVector(U, x_dwt, i);
        insertVector(U_inv, x_idwt, i);
    }

    Eigen::MatrixXd A = U*(U.transpose());
    Eigen::MatrixXd B = U_inv.transpose()*U_inv;
    
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            A(i,j) = (std::abs(A(i,j) < eps))? 0 : A(i,j);
            B(i,j) = (std::abs(A(i,j) < eps))? 0 : B(i,j);
        }
    }   
     
    bool success_DWT = true;
    bool success_IDWT = true;
     
    // Test unitary DWT
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (i != j) {
                success_DWT = (success_DWT and std::abs(A(i,j)) < eps);
                success_IDWT = (success_IDWT and std::abs(B(i,j)) < eps);
            } else {
                success_DWT = (success_DWT  and std::abs(A(i,j)-1) < eps);
                success_IDWT = (success_IDWT  and std::abs(B(i,j)-1) < eps);
            }
        }
    }   
     
    const char *result1 = (success_DWT) ? "\e[32mPassed\e[0m" : "\e[31mFailed\e[0m"; 
    const char *result2 = (success_IDWT) ? "\e[32mPassed\e[0m" : "\e[31mFailed\e[0m"; 
    std::cout << result1 << ": Test for unitarity DWT,  vm: " << vm << std::endl;    
    std::cout << result2 << ": Test for unitarity IDWT, vm: " << vm << std::endl;    
     
}





void printAllHfilters() {
    Filter DB_h = get_DB2();
    
    std::cout << "DB2_h" << std::endl;
    DB_h.printOrder();
        
    
    DB_h = get_DB3();
    
    std::cout << "DB3_h" << std::endl;
    DB_h.printOrder();
        
    
    DB_h = get_DB4();
    
    std::cout << "DB4_h" << std::endl;
    DB_h.printOrder();
        
    
    DB_h = get_DB5();
    
    std::cout << "DB5_h" << std::endl;
    DB_h.printOrder();
        
    
    DB_h = get_DB6();
    
    std::cout << "DB6_h" << std::endl;
    DB_h.printOrder();
    
     
    DB_h = get_DB7();
    
    std::cout << "DB7_h" << std::endl;
    DB_h.printOrder();
    
    DB_h = get_DB8();
    
    std::cout << "DB8_h" << std::endl;
    DB_h.printOrder();
    
    DB_h = get_DB9();
    
    std::cout << "DB9_h" << std::endl;
    DB_h.printOrder();
    
    DB_h = get_DB10();
    
    std::cout << "DB10_h" << std::endl;
    DB_h.printOrder();
}

void printAllGfilters() {
    Filter DB_h = get_DB2();
    Filter DB_g = createG(DB_h);
    
    std::cout << "DB2_g" << std::endl;
    DB_g.printOrder();
        
    
    DB_h = get_DB3();
    DB_g = createG(DB_h);
    
    std::cout << "DB3_g" << std::endl;
    DB_g.printOrder();
        
    
    DB_h = get_DB4();
    DB_g = createG(DB_h);
    
    std::cout << "DB4_g" << std::endl;
    DB_g.printOrder();
        
    
    DB_h = get_DB5();
    DB_g = createG(DB_h);
    
    std::cout << "DB5_g" << std::endl;
    DB_g.printOrder();
        
    
    DB_h = get_DB6();
    DB_g = createG(DB_h);
    
    std::cout << "DB6_g" << std::endl;
    DB_g.printOrder();
    
     
    DB_h = get_DB7();
    DB_g = createG(DB_h);
    
    std::cout << "DB7_g" << std::endl;
    DB_g.printOrder();
    
    DB_h = get_DB8();
    DB_g = createG(DB_h);
    
    std::cout << "DB8_g" << std::endl;
    DB_g.printOrder();
    
    DB_h = get_DB9();
    DB_g = createG(DB_h);
    
    std::cout << "DB9_g" << std::endl;
    DB_g.printOrder();
    
    DB_h = get_DB10();
    DB_g = createG(DB_h);
    
    std::cout << "DB10_g" << std::endl;
    DB_g.printOrder();

}




void test_one_res_signal1() {
    std::vector<double> x = get_testsignal1();    
    std::cerr << "\nTesting signal 1\n";
    test_one_res_signal(x, 1e-9);
}

void test_one_res_signal2() {
    std::vector<double> x = get_testsignal2();    
    std::cerr << "\nTesting signal 2\n"; 
    test_one_res_signal(x, 1e-9);
}


void test_one_res_signal(std::vector<double> & x, double eps) {
    const int N = x.size();
    std::list<Filter> H;
    H.push_back(get_DB2());
    H.push_back(get_DB3());
    H.push_back(get_DB4());
    H.push_back(get_DB5());
    H.push_back(get_DB6());
    H.push_back(get_DB7());
    H.push_back(get_DB8());
    H.push_back(get_DB9());
    H.push_back(get_DB10());
    
    int cnt_DB = 2;
    
    for (std::list<Filter>::iterator it = H.begin(); it != H.end(); ++it) {
        
        Filter DB_h = *it;
        Filter DB_g = createG(DB_h);
        
        std::vector<double> x_dwt = DWTPeriodicOneRes(x, DB_h, DB_g);
        std::vector<double> x_rec = IDWTPeriodicOneRes(x_dwt, DB_h, DB_g);
        
        // Estimate the l_1-error
        double sum = 0;
        for (int i = 0; i < N; i++) {
            sum += std::abs(x[i] - x_rec[i]);
        }
        
        const char *result = (sum < eps) ? "\e[32mPassed\e[0m" : "\e[31mFailed\e[0m";

        std::cerr << "Test DB" << cnt_DB << ": " << result << std::endl;
        cnt_DB++;
        if (sum > eps) {
            std::cerr << "sum: " << sum << std::endl;    
        }
        
        
        //assert(sum < eps);
         
    }
    
}

void test_one_res_signal_template(std::vector<double> &x, double *y, double eps) {
    
    const size_t N = x.size();
    
    std::list<Filter> H;
    H.push_back(get_DB2());
    H.push_back(get_DB3());
    H.push_back(get_DB4());
    H.push_back(get_DB5());
    H.push_back(get_DB6());
    H.push_back(get_DB7());
    H.push_back(get_DB8());
    H.push_back(get_DB9());
    H.push_back(get_DB10());
    
    int cnt_DB = 2;
    
    double z[N];
    double w[N];
    
    for (std::list<Filter>::iterator it = H.begin(); it != H.end(); ++it) {

        Filter DB_h = *it;
        Filter DB_g = createG(DB_h);
        
        std::vector<double> x_dwt = DWTPeriodicOneRes(x, DB_h, DB_g);
        std::vector<double> x_rec = IDWTPeriodicOneRes(x_dwt, DB_h, DB_g);
        
        std::memcpy(z, y, sizeof(double)*N);
        DWTPeriodicOneRes< double >(z, N, DB_h, DB_g); 
        
        std::memcpy(w, z, sizeof(double)*N);
        IDWTPeriodicOneRes< double >(w, N, DB_h, DB_g); 
        
        double sumDWT = 0;
        double sumIDWT = 0;
        
        // Estimate the difference
        for (int i = 0; i < N; i++) {
            sumDWT += std::abs(x_dwt[i] - z[i]);
            sumIDWT += std::abs(x_rec[i] - w[i]);
        }
        
        const char *result = (sumDWT < eps) and (sumDWT < eps) ?"\e[32mPassed\e[0m" : "\e[31mFailed\e[0m"; 

        std::cerr << "Test template one res DB" << cnt_DB << ": " << result << std::endl;
        cnt_DB++;
        if ((sumDWT > eps) and (sumIDWT > eps)) {
            std::cerr << "sumDWT: " << sumDWT << ", sumIDWT: " << sumIDWT << std::endl;    
        }
        
    }
    
}

void test_signal_template(std::vector<double> &x, double *y, int nres, double eps) {
     
    const size_t N = x.size();
     
    std::list<Filter> H;
    H.push_back(get_DB2());
    H.push_back(get_DB3());
    H.push_back(get_DB4());
    H.push_back(get_DB5());
    H.push_back(get_DB6());
    H.push_back(get_DB7());
    H.push_back(get_DB8());
    H.push_back(get_DB9());
    H.push_back(get_DB10());
     
    int cnt_DB = 2;
     
    double z[N];
    double w[N];
     
    for (std::list<Filter>::iterator it = H.begin(); it != H.end(); ++it) {
         
        Filter DB_h = *it;
        Filter DB_g = createG(DB_h);
         
        std::vector<double> x_dwt = DWTPeriodic(x, DB_h, DB_g, nres);
        std::vector<double> x_rec = IDWTPeriodic(x_dwt, DB_h, DB_g, nres);
         
        std::memcpy(z, y, sizeof(double)*N);
        DWTPeriodic< double >(z, N, DB_h, DB_g, nres); 
         
        std::memcpy(w, z, sizeof(double)*N);
        IDWTPeriodic< double >(w, N, DB_h, DB_g, nres); 
         
        double sumDWT = 0;
        double sumIDWT = 0;
         
        // Estimate the difference
        for (int i = 0; i < N; i++) {
            sumDWT += std::abs(x_dwt[i] - z[i]);
            sumIDWT += std::abs(x_rec[i] - w[i]);
        }
         
        const char *result = (sumDWT < eps) and (sumIDWT < eps) ?"\e[32mPassed\e[0m" : "\e[31mFailed\e[0m"; 
         
        std::cerr << "Test template  DB" << cnt_DB << ": " << result << std::endl;
        cnt_DB++;
        if ((sumDWT > eps) or (sumIDWT > eps)) {
            std::cerr << "sumDWT: " << sumDWT << ", sumIDWT: " << sumIDWT << std::endl;    
        }
    }
}


void test_signal_template() {
    
    const double eps = 1e-9;
    
    std::vector<double> x = get_testsignal1();    
    
    const size_t N = x.size();
    double *y = new double[N];
    //std::memcpy(y, &x[0], sizeof(double)*N);
    for (int i = 0; i < N; i++) {
        y[i] = x[i];
    } 
     
    test_signal_template(x,y, 4, eps);
     
}


void test_one_res_template() {
    const double eps = 1e-9;

    std::vector<double> x = get_testsignal1();    
    
    const size_t N = x.size();
    double *y = new double[N];
    //std::memcpy(y, &x[0], sizeof(double)*N);
    for (int i = 0; i < N; i++) {
        y[i] = x[i];
    } 
    
    test_one_res_signal_template(x,y, eps);
        
}


void test_signal1() {
    std::vector<double> x = get_testsignal1();    
    std::cerr << "\nTesting signal 1 all resolutions\n"; 
    test_signal(x, 1e-9);
}

void test_signal2() {
    std::vector<double> x = get_testsignal2();    
    std::cerr << "\nTesting signal 2 all resolutions\n"; 
    test_signal(x, 1e-9);
}

void test_signal(std::vector<double> & x, double eps) {
    
    const int N = x.size();
    const int nres = int (round(log2(double(N)) + 1e-5) );
    
    std::list<Filter> H;
    H.push_back(get_DB2());
    H.push_back(get_DB3());
    H.push_back(get_DB4());
    H.push_back(get_DB5());
    H.push_back(get_DB6());
    H.push_back(get_DB7());
    H.push_back(get_DB8());
    H.push_back(get_DB9());
    H.push_back(get_DB10());
    
    int cnt_DB = 2;
    
    for (std::list<Filter>::iterator it = H.begin(); it != H.end(); ++it) {
        
        Filter DB_h = *it;
        Filter DB_g = createG(DB_h);
        
        std::vector<double> x_dwt = DWTPeriodic(x, DB_h, DB_g, nres);
        std::vector<double> x_rec = IDWTPeriodic(x_dwt, DB_h, DB_g, nres);
        
        
        // Estimate the l_1-error
        double sum = 0;
        for (int i = 0; i < N; i++) {
            sum += std::abs(x[i] - x_rec[i]);
        }
        
        const char *result = (sum < eps) ? "\e[32mPassed\e[0m" : "\e[31mFailed\e[0m";

        std::cerr << "Test DB" << cnt_DB << " all res: " << result << std::endl;
        cnt_DB++;
        if (sum > eps) {
            std::cerr << "sum: " << sum << std::endl;    
        }
        //assert(sum < eps);
    }
    
}



std::vector<double> get_testsignal1() {
    
    std::vector<double> x;
    x.push_back(0.43289123);   // 1
    x.push_back(1.833543423);  // 2
    x.push_back(4.32892323);   // 3
    x.push_back(-5.532423);    // 4 
    x.push_back(0.1231231323); // 5
    x.push_back(0.543212323);  // 6
    x.push_back(4.43223);      // 7
    x.push_back(6.43223214);   // 8
    x.push_back(9.4432223214); // 9
    x.push_back(3.24321);      // 10
    x.push_back(-6.543134);    // 11
    x.push_back(-3.231643123); // 12
    x.push_back(2.645213413);  // 13
    x.push_back(9.453215431);  // 14
    x.push_back(-10.6452413);  // 15
    x.push_back(4.32343214);   // 16
    
    return x;

} 

std::vector<double> get_testsignal2() {
    
    std::vector<double> x;
    x.push_back(4.43543123);   // 1
    x.push_back(1.833543423);  // 2
    x.push_back(4.32892323);   // 3
    x.push_back(-5.532423);    // 4 
    x.push_back(0.1231231323); // 5
    x.push_back(0.543212323);  // 6
    x.push_back(4.43223);      // 7
    x.push_back(6.43223214);   // 8
    x.push_back(4.4625642214); // 9
    x.push_back(3.24321);      // 10
    x.push_back(-6.543134);    // 11
    x.push_back(-3.231643123); // 12
    x.push_back(2.645213413);  // 13
    x.push_back(3.462432431);  // 14
    x.push_back(-10.78322413);  // 15
    x.push_back(-4.32343214);   // 16
    
    return x;
    
}

std::vector<double> get_testsignal3() {
    
    std::vector<double> x;
    x.push_back(-4); // 0
    x.push_back(-6); // 1
    x.push_back( 5); // 2
    x.push_back(-2); // 3
    x.push_back( 6); // 4
    x.push_back(10); // 5
    x.push_back(-3); // 6
    x.push_back( 6); // 7
    x.push_back( 2); // 8
    x.push_back(-2); // 9
    x.push_back(-7); // 10
    x.push_back( 1); // 11
    x.push_back( 3); // 12
    x.push_back( 7); // 13
    x.push_back(12); // 14
    x.push_back(-1); // 15
    
    return x;
}


