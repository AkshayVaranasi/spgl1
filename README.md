# SPGL1 
This repository contains a C++ implementation of the 
[SPGL1 algorithm](http://www.cs.ubc.ca/~mpf/spgl1/index.html "Matlab SPGL1 package"). 
It is based on the work of E. van den Berg and M. P. Friedlander in their paper
*Probing the Pareto frontier for basis pursuit solutions*, SIAM J. on Scientific
Computing, 31(2):890-912, November 2008.

## Overview of the algorithm

The SPGL1 algorithm solves the optimization problem

```
minimize ||z||_1   subject to ||Az - b||_2 <= sigma
```

for applications where the matrix `A` is not stored in memory. Typical examples 
of matrices which are only given implicitly as O(N log N) operators, are
the fast-Fourier transform (FFT), the Walsh-Hadamard transform (WHT) and
discrete wavelet transform (DWT). 

### Application
In applications such as *compressive sensing*, the above problem is solved
in the process of recovering large signals. For such signals it is critical to
use fast operators, rather than dens matrices. As an example consider the
process of recovering a 1024 x 1024 image. This require the vector `z` to be of
length 2^20.  Using a subsampling rate of 25%, this implies that the size of
the matrix `A` should be 2^18 x 2^20. If one used double floating point
arithmetic the size of a densely stored matrix would the require  2 TB of
memory! Thus the need for an algorithm handling these fast operators is quite
obvious. 

## Current state of the code
The algorithm is currently working, but it might contain minor bugs.  For the parts
of the code which can be easily tested, there have been written test functions
to rule out the most obvious of these bugs. The code has not yet been optimized
and parts of it is poorly documented. The performance of the algorithm is highly
dependent on the choice of the parameters involved. To ensure stability and
high performance one also needs to find reasonable choices for these parameters.

This repository contains the main algorithm in the folder *spgl1*. A simple and
very slow implementation of the discrete wavelet transform using Daubechies
wavelets can be found in the folder *wavelets*. In addition the repository
contains the submodule *fastwht*, with a fast implementation of the
Walsh-Hadamard transform.


### Dependencies
The algorithm itself does not depend on any third party code, neither does the 
Hadamard and wavelet transforms. Some of the current test program does however 
depend on [OpenCV](http://opencv.org) and 
[Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page). 


## Example
The algorithm has been used to subsample the image *lena* with a Hadamard matrix, 
using a subsampling rate of 25%. The original and recovered image can been seen below

** Original ** 

![Alt text](spgl1/pictures/lena1024.png "Original")

** Recovered image **

![Alt text](spgl1/pictures/im_rec_subsamp_25_uniform_random.png "Reconstructed image")

## Contact 
For any questions concerning the code please contact Vegard Antun at
vegarant@student.matnat.uio.no